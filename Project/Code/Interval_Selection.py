#!/usr/bin/python

"""MSc_Project script. For selecting only intervals of a certain length or less 
and outputting the resultant data."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math



#####
# Functions
#####

# A function to combine data for a given period on a given date so that a single 
# distance and time coverage er period per day is generated
def single_period_generator(input_df):

	print ('Condensing data.')
	
	# Set Species, Individual, Tag, Date, Period cols to strings
	input_df.loc[:, 'Species'] = input_df.loc[:, 'Species'].astype('str')
	input_df.loc[:, 'Individual'] = input_df.loc[:, 'Individual'].astype('str')
	input_df.loc[:, 'Tag'] = input_df.loc[:, 'Tag'].astype('str')
	input_df.loc[:, 'Date'] = input_df.loc[:, 'Date'].astype('str')
	input_df.loc[:, 'Period'] = input_df.loc[:, 'Period'].astype('str')

	# Conbine columns to generate a UniqueID for subsetting
	input_df.loc[:, 'UniqueID'] = input_df.loc[:, 'Species'] + input_df.loc[:, 'Individual'] + input_df.loc[:, 'Tag'] + input_df.loc[:, 'Date'] + input_df.loc[:,'Period']

	# Crete object to store UniqueIDs in
	uniqueIDs = sorted(set(input_df['UniqueID']))

	nrows = len(uniqueIDs)

	output_df = pd.DataFrame(pd.np.empty((nrows, 14)) * pd.np.nan)

	row_count = 0

	# Subset df by UniqueID
	for i in uniqueIDs:
		unique_sub_df = input_df.loc[input_df.loc[:, 'UniqueID'] == i]

		# Reset indexes of subset
		unique_sub_df = unique_sub_df.reset_index()

		# Extract data needed from subset
		output_df.loc[row_count, 0:13] = [unique_sub_df.loc[0, 'Species'],
										unique_sub_df.loc[0, 'Individual'],
										unique_sub_df.loc[0, 'Tag'],
										unique_sub_df.loc[0, 'Sensor_Type'],
										unique_sub_df.loc[0, 'Date'],
										unique_sub_df.loc[0, 'Period'],
										unique_sub_df.loc[0, 'Month'], 
										unique_sub_df.loc[0, 'Season'],
										unique_sub_df.loc[0, 'Location'],
										sum(unique_sub_df.loc[:, 'New_Interval_sec']),
										sum(unique_sub_df.loc[:, 'New_Distance_m']),
										str(list(unique_sub_df.loc[:, 'New_TS1'])),
										str(list(unique_sub_df.loc[:, 'New_TS2'])),
										unique_sub_df.loc[0, 'Reference']]


		# Add 1 to row counter for next loop
		row_count = row_count + 1

	# Name columns of df
	output_df.columns = ['Species', 'Individual', 'Tag', 'Sensor_Type', 'Date',
						'Period', 'Month', 'Season', 'Location', 'Time_Covered_sec', 'Distance_Covered_m',
						'Aligned_TS1_List', 'Aligned_TS2_List', 'Reference']


	# Return processed df 
	return (output_df)



# A function to restrict the aligned interval data to only intervals whose 
# original interval was 4hrs or under
def under_4_hours(filepath, file):

	print ('Restricting ' + file + ' to orignal intervals 4 hours or under.')

	# Load file
	input_df = pd.read_csv(filepath, sep = ',')

	# Restrict to Orig_Interval_sec <= 4 hrs (14400 sec)
	reduced_df = input_df.loc[input_df.loc[:, 'Orig_Interval_sec'] <= 14400]

	# Process reduced_df
	out_df = single_period_generator(input_df = reduced_df)

	# Specify out_dat_dir
	out_dat_dir = '../Data/GPS_Data/Aligned_sub_4hrs/'
	if not os.path.exists(out_dat_dir):
		os.makedirs(out_dat_dir)	

	# Secify output filepath
	out_fp = out_dat_dir + file

	# Save file
	out_df.to_csv(out_fp, encoding = 'utf-8')



#####
# Main Code
#####

# Establish path to input data directory
in_dat_dir = '../Data/GPS_Data/Aligned_Intervals/'

# Create list of files to process
file_list = os.listdir(in_dat_dir)


print ('Running Interval_Assignment.py' + '\n')

# Loop through files and process
for f in file_list:
	print ('Processing file: ' + f)
	in_fp = in_dat_dir + f
	under_4_hours(filepath = in_fp, file = f)
	print ('Finished processing file: ' + f)
	print ('\n')

print ('\n')