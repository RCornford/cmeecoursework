#!/usr/bin/python

"""MSc_Project script. For assigning seasons to months."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math



#####
# Functions
#####

# Function for adding month and season columns to df
def season_addition(input_file):

	print ('Processing file: ' + input_file)

	# Create file path
	filepath = dat_dir + input_file

	# Load data file
	input_df = pd.read_csv(filepath, sep = ',')

	# Ensure Month is in float format
	input_df['Month'] = input_df['Month'].astype('float')

	# Set Reference, Species, Individual, Month cols to str
	input_df['Reference'] = input_df['Reference'].astype('str')
	input_df['Species'] = input_df['Species'].astype('str')
	input_df['Individual'] = input_df['Individual'].astype('str')
	input_df['Month'] = input_df['Month'].astype('str')

	# Create season col
	input_df.loc[:, 'Season'] = 'NA'


	# Depending on which file is inputted, process in a particular way
	if input_file == 'Movebank_African_Elephant.csv':

		# Create UniqueID using Study, Individual, Month
		input_df['UniqueID'] = input_df.loc[:, 'Reference'] + input_df.loc[:, 'Individual'] + input_df.loc[:, 'Month']

		# Create a list of unique entries in UniqueID col
		uniqueIDs = sorted(set(input_df['UniqueID']))

		for i in uniqueIDs:
			# unique_sub_df = input_df.loc[input_df.loc[:, 'UniqueID'] == i]

			# # Calc length of subset
			# nrow = len(unique_sub_df)

			# Extract season from Season_Data.csv
			season = season_df.loc[season_df.loc[:, 'UniqueID_Elephant'] == i, 'Season'].iloc[0]

			# Extract location data from Season_Data.csv
			location = season_df.loc[season_df.loc[:, 'UniqueID_Elephant'] == i, 'Location'].iloc[0]

			# Add season and location data to input_df
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Season'] = season
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Location'] = location

		# Remove UniqueID col?
		del input_df['UniqueID']
		if 'Unnamed: 0' in input_df.columns:
			del input_df['Unnamed: 0']
		if 'Unnamed: 0.1' in input_df.columns:
			del input_df['Unnamed: 0.1']
		if 'Unnamed: 0.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1']
		if 'Unnamed: 0.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1']		
		if 'Unnamed: 0.1.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1.1']

		# Save df 
		input_df.to_csv(filepath, encoding = 'utf-8')


	elif input_file == 'Movebank_Multiple_Species.csv':

		# Create UniqueID using Study, Species, Month
		input_df['UniqueID'] = input_df.loc[:, 'Reference'] + input_df.loc[:, 'Species'] + input_df.loc[:, 'Month']

		# Create a list of unique entries in UniqueID col
		uniqueIDs = sorted(set(input_df['UniqueID']))

		for i in uniqueIDs:
			# print (i)
			# Extract season from Season_Data.csv
			season = season_df.loc[season_df.loc[:, 'UniqueID_Multispecies'] == i, 'Season'].iloc[0]
			# print (season)

			# Extract location data from Season_Data.csv
			location = season_df.loc[season_df.loc[:, 'UniqueID_Multispecies'] == i, 'Location'].iloc[0]

			# Add season and location data to input_df
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Season'] = season
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Location'] = location

		# Remove UniqueID col?
		del input_df['UniqueID']
		if 'Unnamed: 0' in input_df.columns:
			del input_df['Unnamed: 0']
		if 'Unnamed: 0.1' in input_df.columns:
			del input_df['Unnamed: 0.1']
		if 'Unnamed: 0.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1']
		if 'Unnamed: 0.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1']		
		if 'Unnamed: 0.1.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1.1']

		# Save df 
		input_df.to_csv(filepath, encoding = 'utf-8')


	else:
		# Create UniqueID using Study,  Month
		input_df['UniqueID'] = input_df.loc[:, 'Reference'] + input_df.loc[:, 'Month']

		# Create a list of unique entries in UniqueID col
		uniqueIDs = sorted(set(input_df['UniqueID']))

		for i in uniqueIDs:
			# print (i)
			# Extract season from Season_Data.csv
			season = season_df.loc[season_df.loc[:, 'UniqueID_General'] == i, 'Season'].iloc[0]
			# print (season)

			# Extract location data from Season_Data.csv
			location = season_df.loc[season_df.loc[:, 'UniqueID_General'] == i, 'Location'].iloc[0]

			# Add season data to input_df
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Season'] = season
			input_df.loc[input_df.loc[:, 'UniqueID'] == i, 'Location'] = location

		# Remove UniqueID col?
		del input_df['UniqueID']
		if 'Unnamed: 0' in input_df.columns:
			del input_df['Unnamed: 0']
		if 'Unnamed: 0.1' in input_df.columns:
			del input_df['Unnamed: 0.1']
		if 'Unnamed: 0.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1']
		if 'Unnamed: 0.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1']		
		if 'Unnamed: 0.1.1.1.1' in input_df.columns:
			del input_df['Unnamed: 0.1.1.1.1']	

		# Save df 
		input_df.to_csv(filepath, encoding = 'utf-8')



#####
# Main Code
#####

# Specify input data dir
dat_dir = '../Data/GPS_Data/Aligned_Intervals/'

# Load seasonal data
season_df = pd.read_csv('../Data/GPS_Data/Season_Data.csv', sep = ',')

# Ensure Month is in float format
season_df['Month'] = season_df['Month'].astype('float')

# Convert Reference, Species, Individuals and Montch columns to strings
season_df['Reference'] = season_df['Reference'].astype('str')
season_df['Species'] = season_df['Species'].astype('str')
season_df['Individual'] = season_df['Individual'].astype('str')
season_df['Month'] = season_df['Month'].astype('str')

# Create UniqueID columns for the different situations
season_df['UniqueID_Elephant'] = season_df.loc[:, 'Study'] + season_df.loc[:, 'Individual'] + season_df.loc[:, 'Month']
season_df['UniqueID_Multispecies'] = season_df.loc[:, 'Study'] + season_df.loc[:, 'Species'] + season_df.loc[:, 'Month']
season_df['UniqueID_General'] = season_df.loc[:, 'Study'] + season_df.loc[:, 'Month']
# print (season_df['UniqueID_General'])


# List files to process
filelist = os.listdir(dat_dir)

print ('Running Season_Add.py' + '\n')
# Loop through files and process
for f in filelist:
	season_addition(input_file = f)
print ('\n')