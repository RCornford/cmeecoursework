#!/usr/bin/python

"""MSc_Project script. For processing the ZoaTrack files to give output 
equivalent to that of GPS_to_Distance.py."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math



# Function for processing ZoaTrack file
def data_processing(ZoaTrack_file):

	# Create df to store output
	Output_df = pd.DataFrame()

	print ('Processing file: ' + ZoaTrack_file)

	# Create filepath
	filepath = raw_dat_dir + ZoaTrack_file

	# Load ZoaTrack data
	zoa_df = pd.read_csv(filepath, sep = ',')

	# Count number of unique subsets
	N_subs = len(sorted(set(zoa_df['animal_id'])))

	# Initiaite a subset counter
	sub_count = 1

	# Subset Zoa file by animal_id
	for i in sorted(set(zoa_df['animal_id'])):
		ani_sub_df = zoa_df.loc[zoa_df.loc[:, 'animal_id'] == i]


		print ('Processing subset ' + str(sub_count) + ' out of ' + str(N_subs))

		# Add 1 to sub_count
		sub_count = sub_count + 1

		# Create temp_df to add subset data to
		temp_df = pd.DataFrame()

		# Sort subset by detection_index
		ani_sub_df = ani_sub_df.sort_values(['detection_index'], ascending = True)

		# Reset indexexs
		ani_sub_df = ani_sub_df.reset_index()

		# Create indexes of subset, exluding final row
		index_vals = ani_sub_df.index[[range(0, len(ani_sub_df)-1)]]

		for k in index_vals:

			sp_id = ani_sub_df.loc[k, 'species']
			indiv_id = ani_sub_df.loc[k, 'animal_id']
			tag_id = ani_sub_df.loc[k, 'animal_id']
			sensor_type = ani_sub_df.loc[k, 'sensor_type']
			timestamp1 = ani_sub_df.loc[k, 'detectiontime']
			timestamp2 = ani_sub_df.loc[k+1, 'detectiontime']
			interval = ani_sub_df.loc[k+1, 'step_duration']
			distance = ani_sub_df.loc[k+1, 'step_distance']
			speed = distance/interval
			ref = ani_sub_df.loc[k, 'study']

			# Collate data into row
			data_row = pd.DataFrame([[sp_id, indiv_id, tag_id, sensor_type, timestamp1, timestamp2, interval, distance, speed, ref]],
									columns = ['Species', 'Individual', 'Tag', 'Sensor_Type', 'Timestamp1', 'Timestamp2', 'Interval_sec', 'Distance_m', 'Speed_ms', 'Reference'])

			# Add row to temp_df
			temp_df = temp_df.append(data_row, ignore_index = True)


		# Add temp_df to Output_df
		Output_df = Output_df.append(temp_df, ignore_index = True)

	# Further process Output_df to remove rows where speed > max_accepted_speed
	# Extract max accepted speed from df
	max_accept_speed_kmh = max_obs_speed_df.loc[max_obs_speed_df.loc[:, 'Species'] == Output_df.loc[0, 'Species'], 'Max_Speed'].iloc[0]
	print ('Species: ' + ani_sub_df.loc[0, 'species'])
	print ('Maximum accepted speed in km/h: ' + str(max_accept_speed_kmh))
	max_accept_speed_ms = max_accept_speed_kmh*1000/60/60
	print ('Maximum accepted speed in m/s: ' + str(max_accept_speed_ms))
	
	Output_df = Output_df.loc[Output_df.loc[:, 'Speed_ms'] <= max_accept_speed_ms]

	# Finally, remove rows where intervals = 0 or > 24 hrs
	Output_df = Output_df.loc[Output_df.loc[:, 'Interval_sec'] < 86400]
	Output_df = Output_df.loc[Output_df.loc[:, 'Interval_sec'] > 0]

	# Create output filepath
	out_fp = out_dat_dir + ZoaTrack_file

	# Save Ouput_df
	Output_df.to_csv(out_fp, encoding = 'utf-8')

	print ('Finished processing file: ' + ZoaTrack_file)




# Establish path to directory containing data to read in
raw_dat_dir = '../Data/GPS_Data/ZoaTrack_Data/'

# Establish path to output directory
out_dat_dir = '../Data/GPS_Data/Distance_Data/'

# Load max_obs_speed data
max_obs_speed_df = pd.read_csv('../Data/GPS_Data/Max_Speed_Data.csv', sep = ',')

# List all files in orig_data_directory
file_list = os.listdir(raw_dat_dir)

print ('Running Zoa_to_Distance.py' + '\n')
for f in file_list:
	data_processing(ZoaTrack_file = f)
print ('\n')