#!/usr/bin/python

"""MSc_Project script. For calculating the distance travelled per day from the 
houly data."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math



def daily_distance_processing(input_directory, input_file, output_directory):

	# Create filepath
	filepath = input_directory + input_file

	# Load data
	input_df = pd.read_csv(filepath, sep = ',')

	# Convert Species, Individual, Tag, Season and Date to string
	input_df['Species'] = input_df.loc[:, 'Species'].astype('str')
	input_df['Individual'] = input_df.loc[:, 'Individual'].astype('str')
	input_df['Tag'] = input_df.loc[:, 'Tag'].astype('str')
	input_df['Date'] = input_df.loc[:, 'Date'].astype('str')

	# Create UniqueID col
	input_df['UniqueID'] = input_df.loc[:, 'Species'] + input_df.loc[:, 'Individual'] + input_df.loc[:, 'Tag'] + input_df.loc[:, 'Date']

	uniqueIDs = sorted(set(input_df.loc[:, 'UniqueID']))

	# Create output dataframe (length of uniqueIDs, 11 cols)
	output_df = pd.DataFrame(pd.np.empty((len(uniqueIDs), 13)) * pd.np.nan)


	# Subset by UniqueID
	for i in range(0, len(uniqueIDs)):
		sub_df = input_df.loc[input_df.loc[:, 'UniqueID'] == uniqueIDs[i]]

		# Reset index
		sub_df = sub_df.reset_index()

		# Calculate total distance and time covered by data per day
		total_dist = sum(sub_df.loc[:, 'Distance_Covered_m'])
		total_time = sum(sub_df.loc[:, 'Time_Covered_sec'])
		n_periods = len(sub_df)
		daily_dist_est = total_dist/total_time*86400

		# Add data row to output_df
		output_df.loc[i, 0:12] = [sub_df.loc[0, 'Species'],
									sub_df.loc[0, 'Individual'],
									sub_df.loc[0, 'Tag'],
									sub_df.loc[0, 'Sensor_Type'],
									sub_df.loc[0, 'Location'],
									sub_df.loc[0, 'Month'],
									sub_df.loc[0, 'Date'],
									sub_df.loc[0, 'Season'],
									# sub_df.loc[0, 'Resource_Level'],
									total_dist,
									total_time,
									n_periods,
									daily_dist_est,
									sub_df.loc[0, 'Reference']]



	# Name columns
	output_df.columns = ['Species', 'Individual', 'Tag', 'Sensor_Type', 'Location',
						'Month', 'Date', 'Season', 
						'Total_Distance_m',	'Total_Time_sec', 'N_Periods_Covered', 
						'Daily_Distance_Estimate_m', 'Reference']

	# Save output_df to correct directory 
	out_filepath = output_directory + input_file

	output_df.to_csv(out_filepath, encoding = 'utf-8')




# Specify input directories
in_dat_dir = '../Data/GPS_Data/Aligned_sub_4hrs/'


# Specify output directory
out_dat_dir = '../Data/GPS_Data/Daily_Distances_sub_4hr_Intervals/'
if not os.path.exists(out_dat_dir):
	os.makedirs(out_dat_dir)


# Create list of files to process
file_list = os.listdir(in_dat_dir)

print ('Running Daily_Distance_Processing.py' + '\n')

# Loop through files and process
for f in file_list:
	print ('Processing ' + f + ' where intervals are 2hr or under.')
	daily_distance_processing(input_directory = in_dat_dir, input_file = f, output_directory = out_dat_dir)
	print ('\n')
print ('\n')

