#!/usr/bin/python

"""MSc_Project script. For assigning intervals to regular time periods."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math


#####
# Functions
#####

# Function to check where interval falls in day and assign to hourly period
def assign_interval(input_interval, output_dataframe):

	# Assign some values to objects for easier use	
	Date1 = input_interval['Timestamp1'].date()
	Date2 = input_interval['Timestamp2'].date()
	Time1 = input_interval['Timestamp1'].time()
	Time2 = input_interval['Timestamp2'].time()
	Hour1 = input_interval['Timestamp1'].hour
	Hour2 = input_interval['Timestamp2'].hour	
	Month = input_interval['Timestamp1'].month
	ST1 = pd.to_datetime(str(Date1) + ' ' + str(Hour1) + ':00:00')
	ST2 = pd.to_datetime(str(Date2) + ' ' + str(Hour2) + ':00:00')


	# Multiple conditions to properly asses current interval and assign it correctly
	if Hour1 == Hour2:
		# Current interval is within a 1 hr period, assign as is
		# print ('Within a period, Yay!')

		data = pd.DataFrame([[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type'], 
							Hour1, Date1, Month,
							input_interval['Timestamp1'], input_interval['Timestamp2'], 
							input_interval['Interval_sec'], input_interval['Distance_m'], 
							input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]])

		# print (data)
		
		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif ((Hour2-Hour1 == 1) | (Hour2-Hour1 == -23)) & (input_interval['Timestamp1'] == ST1) & (input_interval['Timestamp2'] == ST2):
		# Current interval runs from 1hr to the next exactyl, assign as is
		# print ('Perfect period')

		data = pd.DataFrame([[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type'], 
							Hour1, Date1, Month,
							input_interval['Timestamp1'], input_interval['Timestamp2'], 
							input_interval['Interval_sec'], input_interval['Distance_m'], 
							input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]])		
		
		# print (data)

		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif ((Hour2-Hour1 == 1) | (Hour2-Hour1 == -23)) & (input_interval['Timestamp2'] == ST2):
		# Current interval runs from within a period to the exact end point, assign as is
		# print ('Within a period!, ends on a period')

		data = pd.DataFrame([[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type'], 
							Hour1, Date1, Month,
							input_interval['Timestamp1'], input_interval['Timestamp2'], 
							input_interval['Interval_sec'], input_interval['Distance_m'], 
							input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]])
		
		# print (data)

		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif (Hour2-Hour1 == 1):
		# Current interval overlaps a period boundary at ST2, split and assign
		# print('Single overlap')

		# Calculate the number of rows need to add to the output_dataframe
		nrow = Hour2-Hour1+1

		# houroverlap = pd.to_datetime(str(D1) + ' ' + str(H2) + ':00:00')

		# Calculate the lengths of the newly created intervals
		interval1 = (ST2 - input_interval['Timestamp1']).total_seconds()
		interval2 = (input_interval['Timestamp2'] - ST2).total_seconds()

		# Create a df of NAs
		data = pd.DataFrame(pd.np.empty((nrow, 17)) * pd.np.nan)

		# Fill the df
		# Data for interval1
		data.loc[0, 0:16] = [input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type'], 
							Hour1, Date1, Month,
							input_interval['Timestamp1'], ST2, interval1, 
							interval1*input_interval['Speed_ms'], input_interval['Speed_ms'], 
							input_interval['Orig_TS1'], input_interval['Orig_TS2'], 
							input_interval['Orig_Interval'], input_interval['Orig_Distance'], 
							input_interval['Reference']]

		# Data for interval2
		data.loc[1, 0:16] = [input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type'], 
							Hour2, Date2, Month,
							ST2, input_interval['Timestamp2'], interval2, 
							interval2*input_interval['Speed_ms'], input_interval['Speed_ms'], 
							input_interval['Orig_TS1'], input_interval['Orig_TS2'], 
							input_interval['Orig_Interval'], input_interval['Orig_Distance'], 
							input_interval['Reference']]
		
		# print (data)

		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif ((Hour2-Hour1 > 1)|((Hour2-Hour1 < 0) & (Hour2-Hour1 > -23))) & (input_interval['Timestamp1'] == ST1) & (input_interval['Timestamp2'] == ST2):
		# Current interval runs over a number of periods, starting and ending on period boundaries
		# Divide equally among periods and assign
		# print ('multiple perfect periods')

		# Calculate the number of rows needed to add to the output_dataframe
		nrow = Hour2-Hour1

		# If H2-H1 is negative because H2 is 0, convert as necessary to ensure positive values
		if nrow < 0:
			nrow = nrow + 24
			Hour2 = 24

		# Create an empty list to fill with period boundaries overlapped
		boundaries = []

		# Create required period boundary timestamps and add to boundaries 
		for i in range(Hour1+1,Hour2):
			boundaries.append(pd.to_datetime(str(Date1) + ' ' + str(i) + ':00:00'))

		# Create a list of periods covered by current interval
		periods = range(Hour1, Hour2)

		# Create df of NAs
		data = pd.DataFrame(pd.np.empty((nrow, 17)) * pd.np.nan)

		# Add data to df
		# Species, Individual, Tag, Sensor_Type
		data.loc[:, 0:3] = [[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type']]]*nrow

		# Periods
		data.loc[:,4] = periods

		# Date
		data.loc[:,5] = [Date1]*nrow

		# Month
		data.loc[:,6] = [Month]*nrow

		# New_Interval_Start_Times 
		data.loc[0,7] = input_interval['Timestamp1']
		data.loc[1:,7] = boundaries

		# New_Interval_End_Times
		data.loc[0:nrow-2,8] = boundaries
		data.loc[nrow-1,8] = input_interval['Timestamp2']


		# New_Interval_sec, New_Distance_m, Speed_ms, Orig_TS1, Orig_TS2, Orig_Interval_sec, Orig_Distance_m, Reference
		data.loc[:,9:16] = [[input_interval['Interval_sec']/nrow, input_interval['Distance_m']/nrow, 
							input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]]*nrow

		# print (data)

		# Add data to output output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif ((Hour2-Hour1 > 1)|((Hour2-Hour1 < 0) & (Hour2-Hour1 > -23))) & (input_interval['Timestamp2'] == ST2):
		# Current interval spans ultiple periods but ends on a boundary
		# Split appropriately and assign
		# print ('Multiple overlps, ends on a period')

		# Calculate the number of rows needed to add to the output_dataframe
		nrow = Hour2-Hour1

		# If H2-H1 is negative becous H2 is 0, convert as necessary to ensure positive values
		if nrow < 0:
			nrow = nrow + 24
			Hour2 = 24

		# Create an empty list to fill with period boundaries overlapped
		boundaries = []

		# Create required period boundary timestamps and add to boundaries
		for i in range(Hour1+1,Hour2):
			boundaries.append(pd.to_datetime(str(Date1) + ' ' + str(i) + ':00:00'))

		# Create a list of periods covered by current interval
		periods = range(Hour1, Hour2)

		# Create df of NAs
		data = pd.DataFrame(pd.np.empty((nrow, 17)) * pd.np.nan)

		# Add data to df
		# Species, Individual, Tag, Sensor_Type
		data.loc[:, 0:3] = [[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type']]]*nrow

		# Periods
		data.loc[:,4] = periods

		# Date
		data.loc[:,5] = [Date1]*nrow

		# Month
		data.loc[:,6] = [Month]*nrow

		# New_Interval_Start_Times
		data.loc[0,7] = input_interval['Timestamp1']
		data.loc[1:,7] = boundaries

		# New_Interval_End_Times
		data.loc[0:nrow-2,8] = boundaries
		data.loc[nrow-1,8] = input_interval['Timestamp2']

		# New_Interval_sec
		data.loc[0,9] = (boundaries[0] - input_interval['Timestamp1']).total_seconds()
		data.loc[1:,9] = [3600]*(nrow-1)

		# New_Distance_m
		data.loc[0,10] = (boundaries[0] - input_interval['Timestamp1']).total_seconds()*input_interval['Speed_ms']
		data.loc[1:,10] = [(3600*input_interval['Speed_ms'])]*(nrow-1)

		# Speed_ms, Orig_TS1, Orig_TS2, Orig_Interval_sec, Orig_Distance_m, Reference
		data.loc[:,11:16] = [[input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]]*nrow

		# print (data)

		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	elif (Hour2 - Hour1 > 1):
		# Current interval overlaps multiple periods
		# Split appropritely and assign
		# print ('Multiple overlaps')
		# n_overlaps = H2 - H1

		# Calculate the number of rows needed to add to the output_dataframe
		nrow = Hour2-Hour1+1

		# Create an empty list to fill with period boundaries overlapped
		boundaries = []

		# Create required period boundary timestamps and add to boundaries
		for i in range(Hour1+1, Hour2+1):
			boundaries.append(pd.to_datetime(str(Date1) + ' ' + str(i) + ':00:00'))

		# Create a list of periods covered by current interval
		periods = range(Hour1, Hour2+1)

		# Create df of NAs
		data = pd.DataFrame(pd.np.empty((nrow, 17)) * pd.np.nan)

		# Add data to df
		# Species, Individual, Tag, Sensor_Type
		data.loc[:, 0:3] = [[input_interval['Species'], input_interval['Individual'], 
							input_interval['Tag'], input_interval['Sensor_Type']]]*nrow

		# Periods
		data.loc[:,4] = periods

		# Date
		data.loc[:,5] = [Date1]*nrow

		# Month
		data.loc[:,6] = [Month]*nrow

		# New_Interval_Start_Times 
		data.loc[0,7] = input_interval['Timestamp1']
		data.loc[1:,7] = boundaries

		# New_Interval_End_Times
		data.loc[0:nrow-2,8] = boundaries
		data.loc[nrow-1,8] = input_interval['Timestamp2']		

		# New_Interval_sec
		data.loc[0,9] = (boundaries[0]-input_interval['Timestamp1']).total_seconds()
		data.loc[1:nrow-2,9] = [3600]*(nrow-2)
		data.loc[nrow-1,9] = (input_interval['Timestamp2']-boundaries[-1]).total_seconds()

		# New_Distances_m 
		data.loc[0,10] = (boundaries[0]-input_interval['Timestamp1']).total_seconds()*input_interval['Speed_ms']
		data.loc[1:nrow-2,10] = [3600*input_interval['Speed_ms']]*(nrow-2)
		data.loc[nrow-1,10] = (input_interval['Timestamp2']-boundaries[-1]).total_seconds()*input_interval['Speed_ms']

		# Speed_ms, Orig_TS1, Orig_TS2, Orig_Interval_sec, Orig_Distance_m, Reference
		data.loc[:,11:16] = [[input_interval['Speed_ms'], input_interval['Orig_TS1'], 
							input_interval['Orig_TS2'], input_interval['Orig_Interval'], 
							input_interval['Orig_Distance'], input_interval['Reference']]]*nrow

		# print (data)

		# Add data to output_dataframe
		output_dataframe = output_dataframe.append(data, ignore_index = True)


	return (output_dataframe)



# Function for processing distance data
def data_processing(Distance_file):

	print ('Processing file: ' + Distance_file)

	# Create filepath
	filepath = in_dat_dir + Distance_file

	# Load Distance df
	Dist_df = pd.read_csv(filepath, sep = ',')


	# Create df for storing output
	Aligned_df = pd.DataFrame()

	# Ignore Allocated_Date col

	# Convert timestamp cols to datetime
	Dist_df['Timestamp1'] = pd.to_datetime(Dist_df['Timestamp1'])
	Dist_df['Timestamp2'] = pd.to_datetime(Dist_df['Timestamp2'])

	# Extract date and time only from timestamps
	Dist_df['Date1'] = Dist_df['Timestamp1'].dt.date
	Dist_df['Date2'] = Dist_df['Timestamp2'].dt.date
	Dist_df['Time1'] = Dist_df['Timestamp1'].dt.time
	Dist_df['Time2'] = Dist_df['Timestamp2'].dt.time
	Dist_df['Hour1'] = Dist_df['Timestamp1'].dt.hour
	Dist_df['Hour2'] = Dist_df['Timestamp2'].dt.hour

	# Add columns to ensure original interval data is retained
	Dist_df['Orig_TS1'] = Dist_df['Timestamp1']
	Dist_df['Orig_TS2'] = Dist_df['Timestamp2']
	Dist_df['Orig_Interval'] = Dist_df['Interval_sec']
	Dist_df['Orig_Distance'] = Dist_df['Distance_m']


	# Convert columns to strings
	Dist_df['Species'] = Dist_df['Species'].astype('str')
	Dist_df['Individual'] = Dist_df['Individual'].astype('str')
	Dist_df['Tag'] = Dist_df['Tag'].astype('str')

	# Create UniqueID col
	Dist_df['UniqueID'] = Dist_df.loc[:, 'Species'] + Dist_df.loc[:, 'Individual'] + Dist_df.loc[:, 'Tag']

	# Count number of unique subsets
	N_subs = len(sorted(set(Dist_df['UniqueID'])))

	# Initiate a subset counter
	sub_count = 1

	# Subset Dist_df by UniqueID
	for i in sorted(set(Dist_df['UniqueID'])):
		Unique_sub_df = Dist_df.loc[Dist_df.loc[:, 'UniqueID'] == i]

		print ('Processing subset ' + str(sub_count) + ' out of ' + str(N_subs))

		# Add 1 to sub_count
		sub_count = sub_count + 1		

		# Create temp_df to store data from subset
		temp_df = pd.DataFrame()

		# Reset indexes
		Unique_sub_df = Unique_sub_df.reset_index()

		# Create object of index values
		index_values = Unique_sub_df.index[[range(0, len(Unique_sub_df))]]

		# Loop through dist_df to assign intervals to specific hourly periods
		for k in index_values:

			interval_data = Unique_sub_df.loc[k, :]

			if interval_data['Date1'] == interval_data['Date2']:
				# print ('Same date!!!')
				temp_df = assign_interval(input_interval = interval_data, output_dataframe = temp_df)
	

			elif (interval_data['Date1'] != interval_data['Date2']) & (interval_data['Timestamp2'] == pd.to_datetime(interval_data['Date2'])):
				# print ('Second Ts is midnight')
				temp_df = assign_interval(input_interval = interval_data, output_dataframe = temp_df)


			elif interval_data['Date1'] != interval_data['Date2']:
				# print ('multiple days')

				# midnight is on second date
				midnight = pd.to_datetime(interval_data['Date2'])

				temp_data = pd.DataFrame(pd.np.empty((2, 14)) * pd.np.nan)

				temp_data.loc[:,0:3] = [[interval_data['Species'], interval_data['Individual'], 
										interval_data['Tag'], interval_data['Sensor_Type']]]*2

				temp_data.loc[0,4] = interval_data['Timestamp1']
				temp_data.loc[1,4] = midnight

				temp_data.loc[0,5] = midnight
				temp_data.loc[1,5] = interval_data['Timestamp2']

				temp_data.loc[0,6] = (midnight - interval_data['Timestamp1']).total_seconds()
				temp_data.loc[1,6] = (interval_data['Timestamp2'] - midnight).total_seconds()

				temp_data.loc[0,7] = (midnight - interval_data['Timestamp1']).total_seconds()*interval_data['Speed_ms']
				temp_data.loc[1,7] = (interval_data['Timestamp2'] - midnight).total_seconds()*interval_data['Speed_ms']

				temp_data.loc[:,8] = [[interval_data['Speed_ms']]]*2

				temp_data.loc[:,9:13] = [[interval_data['Orig_TS1'], interval_data['Orig_TS2'], 
										interval_data['Orig_Interval'], interval_data['Orig_Distance'], 
										interval_data['Reference']]]*2

				# Assign col names
				temp_data.columns = ['Species', 'Individual', 'Tag', 'Sensor_Type',
									'Timestamp1', 'Timestamp2', 'Interval_sec',
									'Distance_m', 'Speed_ms', 'Orig_TS1',
									'Orig_TS2', 'Orig_Interval', 'Orig_Distance', 'Reference']

	
				temp_df = assign_interval(input_interval = temp_data.loc[0,:], output_dataframe = temp_df)
				temp_df = assign_interval(input_interval = temp_data.loc[1,:], output_dataframe = temp_df)
				


		# Finally, add temp_df to Aligned_df 
		Aligned_df = Aligned_df.append(temp_df, ignore_index = True)

	Aligned_df.columns = ['Species', 'Individual', 'Tag', 'Sensor_Type', 'Period',
							'Date', 'Month', 'New_TS1', 'New_TS2', 'New_Interval_sec', 
							'New_Distance_m',  'Speed_ms', 'Orig_TS1', 'Orig_TS2', 
							'Orig_Interval_sec', 'Orig_Distance_m', 'Reference']


	# Create output filepath
	out_fp = out_dat_dir + Distance_file

	# Save file
	Aligned_df.to_csv(out_fp, encoding = 'utf-8')

	print ('Finished processing file: ' + Distance_file)



#####
# Main Code
#####

# Establish path to directory containing data to read in
in_dat_dir = '../Data/GPS_Data/Distance_Data/'

# Establish output directory
out_dat_dir = '../Data/GPS_Data/Aligned_Intervals/'
if not os.path.exists(out_dat_dir):
	os.makedirs(out_dat_dir)

# Create list of files to process
filelist = os.listdir(in_dat_dir)

print ('Running Interval_Assignment.py' + '\n')
# Loop through files and process
for f in filelist:
	data_processing(Distance_file = f)
print ('\n')