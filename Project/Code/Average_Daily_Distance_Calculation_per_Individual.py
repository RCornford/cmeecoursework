#!/usr/bin/python

"""MSc_Project script. For calculating the average distance travelled per day 
per individual per season."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math



# A function to convert season to resource availability (rich v poor)
def resource_availability(input_df):

	# List the rich and poor seasons
	rich_seasons = ['Spring', 'Summer', 'Autumn', 'Wet', 'Summer/Wet', 'Cool']
	poor_seasons = ['Winter', 'Dry', 'Winter/Dry', 'Hot/Dry', 'Cool/Dry']

	# Add resource col to df
	# input_df['Resource_Level'] = 'NA'

	# Subset df by season
	unique_seasons = sorted(set(input_df.loc[:, 'Season']))

	for i in unique_seasons:
		# If seaoson in rich, put rich in resource col
		if i in rich_seasons:
			input_df.loc[input_df.loc[:, 'Season'] == i, 'Resource_Level'] = 'Rich'

		# If season in poor, put poor in resource col
		elif i in poor_seasons:
			input_df.loc[input_df.loc[:, 'Season'] == i, 'Resource_Level'] = 'Poor'


	# Return modified df
	return (input_df)



def average_distance_per_individual(input_directory, input_file, output_dataframe):

	# Create filepath
	filepath = input_directory + input_file

	# Load data
	input_df = pd.read_csv(filepath, sep = ',')

	# Remove rows where Total_Time_sec != 86400
	input_df = input_df.loc[input_df.loc[:, 'Total_Time_sec'] == 86400]

	# Convert Species, Individual and Season to string
	input_df['Species'] = input_df.loc[:, 'Species'].astype('str')
	input_df['Individual'] = input_df.loc[:, 'Individual'].astype('str')
	input_df['Season'] = input_df.loc[:, 'Season'].astype('str')

	input_df['UniqueID'] = input_df.loc[:, 'Species'] + input_df.loc[:, 'Individual'] + input_df.loc[:, 'Season']

	uniqueIDs = sorted(set(input_df.loc[:, 'UniqueID']))

	# Subset by uniqueID
	for i in uniqueIDs:
		sub_df = input_df.loc[input_df.loc[:, 'UniqueID'] == i]

		# Reset indexes
		sub_df = sub_df.reset_index()

		# Calc mean daily distance
		mean_daily_dist = sub_df.loc[:, 'Total_Distance_m'].mean()

		# Extract N of days
		n_days = len(sub_df)

		# Create data row
		data_row = pd.DataFrame([[sub_df.loc[0, 'Species'],
								sub_df.loc[0, 'Individual'],
								sub_df.loc[0, 'Sensor_Type'],
								sub_df.loc[0, 'Location'],
								sub_df.loc[0, 'Season'],
								mean_daily_dist,
								n_days,
								sub_df.loc[0, 'Reference']]])

		# Add row to output_df
		output_dataframe = output_dataframe.append(data_row, ignore_index = True)


	return (output_dataframe)



# Specify input data directories
in_dat_dir = '../Data/GPS_Data/Daily_Distances_sub_4hr_Intervals/'

# Create file list
file_list = os.listdir(in_dat_dir)

# Create output dataframes to store average data per tag per season
av_4hr_df = pd.DataFrame()

print ('Running Average_Daily_Distance_Calculation_per_Individual.py' + '\n')

# Loop through files and process
for f in file_list:
	print ('Calculating average daily distance per individual for ' + f + ' where intervals are 4hrs or under.')
	av_4hr_df = average_distance_per_individual(input_directory = in_dat_dir, input_file = f, output_dataframe = av_4hr_df)
	print ('\n')

# Name columns of average df
av_4hr_df.columns = ['Species', 'Individual', 'Sensor_Type', 'Location',
					'Season', 'Mean_Daily_Distance_m', 'N_whole_days', 'Reference']


# Add Resource_Level col
av_4hr_df = resource_availability(input_df = av_4hr_df)

# Save average df
av_4hr_df.to_csv('../Data/Data_For_Final_Analysis/Sample_Mean_Daily_Distances_sub_4hr_Intervals_Indiv.csv', encoding = 'utf-8')
