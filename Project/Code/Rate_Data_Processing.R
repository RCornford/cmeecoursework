##########
# R script to load and process BMR and Intake rate data
##########

# Clear working environment
rm(list = ls())
graphics.off()

# Require plyr for data manipulation
require(plyr)

#######
# BMR data processing
#######
#####
# Process BMR data from Kolokotrones et al 2010
#####

# Load Kolokotrones data
kolo_orig_df <- read.csv('../Data/Energetic_Rates/Kolocotrones_et_al_2010._Met_data.csv')

# Subset data to only include terrestrial herbivorous mammals
# Exclude Substrate = AQ
kolo_mod_df <- subset(kolo_orig_df, kolo_orig_df$Substrate != 'AQ')

# Subset to only herbivorous terrestrial mammals
herb_diet <- c('B', 'F', 'G', 'L', 'N', 'S')
kolo_mod_df <- subset(kolo_mod_df, kolo_mod_df$Food %in% herb_diet)

# Issue with above is that some non-herbivores remain, as do bats.
# Also retain marsupials so get rid of these.
unwanted_orders <- c('Diprotodontia', 'Chiroptera', 'Carnivora')
kolo_mod_df <- subset(kolo_mod_df, !(kolo_mod_df$Order %in% unwanted_orders))

# Create new df for the terrestrial herbivores, including only necessary columns
kolo_terr_herbs_df <- data.frame('Species' = kolo_mod_df$Genus.Species, 
                                 'Mass_kg' = kolo_mod_df$Mass..g./1000, 
                                 'BMR_J_s' = kolo_mod_df$BMR..W.)

# Save processed Kolokotrones data
write.csv(kolo_terr_herbs_df, '../Data/Energetic_Rates/Kolocotrones_et_al_2010._Terr_Herbs.csv', row.names = F)



#######
# Intake rate data processing
#######
#####
# Proces intake rate data from Muller et al 2012
#####

# Load Muller data
muller_orig_df <- read.csv('../Data/Energetic_Rates/Muller_et_al_2013_Intake_rate_data.csv')

# Remove Marsupials
marsupials <- c('Macropus rufus', 'Macropus giganteus', 'Macropus eugenii', 
                'Macropus robustus', 'Thylogale thetis', 'Vombatus ursinus',
                'Trichosurus vulpecula', 'Lasiorhinus latifrons', 'Lagorchestes hirsutus')

muller_mod_df <- muller_orig_df[!(muller_orig_df$Species %in% marsupials),]

# Create new df to store converted intake data
muller_conv_df <- data.frame('Species' = muller_mod_df$Species,
                             'Mass_kg' = muller_mod_df$Mass_kg,
                             'Intake_J_s' = muller_mod_df$Orig_Intake_Value*22e6/86400,
                             'Diet_Type' = muller_mod_df$Diet_Type)

# Save new df
write.csv(muller_conv_df, '../Data/Energetic_Rates/Muller_et_al_2013_Converted_Intake.csv', row.names = F)

