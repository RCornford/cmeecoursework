#!/usr/bin/python

""" Python script for running MSc project workflow. """

__author__ = 'Richard Cornford'


# Import the subprocess module
import subprocess

#####
# Start by analysing scaling of enrgetic rates and plotting these relations
#####

# Run rate_data_processing.R
subprocess.os.system('Rscript Rate_Data_Processing.R')

# Run scaling_analysis1.R
subprocess.os.system('Rscript Scaling_Analysis.R')

# Run fig_plotting.R
subprocess.os.system('Rscript Fig_Plotting.R')



#####
# Now process the raw tracking data
#####

# Run GPS_to_Distance.py
subprocess.os.system('python GPS_to_Distance.py')

# Run Zoa_to_Distance.py
subprocess.os.system('python Zoa_to_Distance.py')

# Run Interval_Assignment.py
subprocess.os.system('python Interval_Assignment.py')

# Run Season_Add.py
subprocess.os.system('python Season_Add.py')

# Run Interval_Selection.py
subprocess.os.system('python Interval_Selection.py')

# Run Daily_Distance_Processing.py
subprocess.os.system('python Daily_Distance_Processing.py')

# Run Average_Daily_Distance_per_Individual.py
subprocess.os.system('python Average_Daily_Distance_Calculation_per_Individual.py')



#####
# Analyse the daily distance data
#####

# Run Analysis.R
subprocess.os.system('Rscript Final_Analysis.R')


# Run Lit_Activty_Review.R
subprocess.os.system('Rscript Lit_Activity_Review.R')
