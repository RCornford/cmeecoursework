#!/usr/bin/python

"""MSc_Project script. For processing the Movebank GPS files to give distances."""

__author__ = 'Richard Cornford'


# Import the necessary packages
import os
import pandas as pd
import scipy as sc
import math
from geographiclib.geodesic import Geodesic



# Function for processing GPS data and extracting distances
def data_processing(GPS_file):

	# Create Distance_df to store output
	Distance_df = pd.DataFrame()

	print ('Processing file: ' + GPS_file)

	# Create filepath
	filepath = GPS_dat_dir + GPS_file

	# Load the GPS data
	GPS_df = pd.read_csv(filepath, sep = ',')

	# If 'visible' column present, remove rows where value is 'False'
	if 'visible' in GPS_df.columns:
		GPS_df = GPS_df[GPS_df['visible'] != False]

	# Remove rows where GPS data is not available
	GPS_df = GPS_df.loc[pd.isnull(GPS_df.loc[:, 'location-lat']) == False]
	GPS_df = GPS_df.loc[pd.isnull(GPS_df.loc[:, 'location-long']) == False]

	# print (len(GPS_df))

	# Convert timestamp col to datetime64 dtype
 	GPS_df['timestamp'] = pd.to_datetime(GPS_df['timestamp'])

 	# Set Species, Individual and Tag id columns to strings for concatenation
	GPS_df['individual-taxon-canonical-name'] = GPS_df['individual-taxon-canonical-name'].astype('str')
	GPS_df['individual-local-identifier']	= GPS_df['individual-local-identifier'].astype('str')
	GPS_df['tag-local-identifier'] = GPS_df['tag-local-identifier'].astype('str')

	# Create a UniqueID column by merging individual and tag columns
	GPS_df['UniqueID'] = GPS_df.loc[:, 'individual-taxon-canonical-name'] + GPS_df.loc[:, 'individual-local-identifier'] + GPS_df.loc[:, 'tag-local-identifier']
	
	# Count number of unique subsets
	N_subs = len(sorted(set(GPS_df['UniqueID'])))

	# Initiate a subset counter
	sub_count = 1

	# Subset GPS file by UniqueID
	for i in sorted(set(GPS_df['UniqueID'])):
		Unique_sub_df = GPS_df.loc[GPS_df.loc[:, 'UniqueID'] == i]

		print ('Processing subset ' + str(sub_count) + ' out of ' + str(N_subs))

		# Add 1 to sub_count
		sub_count = sub_count + 1

		# Create temp_df to store data from subset
		temp_df = pd.DataFrame()

		# Sort by timestamps
		Unique_sub_df = Unique_sub_df.sort_values(['timestamp'], ascending = True)

		# Reset indexes
		Unique_sub_df = Unique_sub_df.reset_index()

		# Create indexes of subset, exluding final row
		index_vals = Unique_sub_df.index[[range(0, (len(Unique_sub_df)-1))]]

		# Loop through indexes and extract important info
		for k in index_vals:

			sp_id = Unique_sub_df.loc[k, 'individual-taxon-canonical-name']
			indiv_id = Unique_sub_df.loc[k, 'individual-local-identifier']
			tag_id = Unique_sub_df.loc[k, 'tag-local-identifier']
			sensor_type = Unique_sub_df.loc[k, 'sensor-type']
			timestamp1 = Unique_sub_df.timestamp[k]
			timestamp2 = Unique_sub_df.timestamp[k+1]
			time_delt = Unique_sub_df.timestamp[k+1]-Unique_sub_df.timestamp[k]
			interval = time_delt.total_seconds()
			distance = geod.Inverse(Unique_sub_df.loc[k,'location-lat'], Unique_sub_df.loc[k,'location-long'], Unique_sub_df.loc[k+1,'location-lat'], Unique_sub_df.loc[k+1,'location-long'])['s12']
			# speed = distance/interval	
			ref = Unique_sub_df.loc[k, 'study-name']			

			# Collate data into row
			data_row = pd.DataFrame([[sp_id, indiv_id, tag_id, sensor_type, timestamp1, timestamp2, interval, distance, ref]],
									columns = ['Species', 'Individual', 'Tag', 'Sensor_Type', 'Timestamp1', 'Timestamp2', 'Interval_sec', 'Distance_m', 'Reference'])

			# Add row to output_df
			temp_df = temp_df.append(data_row, ignore_index = True)


		# Only process further if temp_df contains values
		if len(temp_df) > 0:

			# Remove rows where interval is 0
			temp_df = temp_df.loc[temp_df.loc[:, 'Interval_sec'] > 0]

			# Calculate average speed of meovement within interval
			temp_df['Speed_ms'] = temp_df.loc[:, 'Distance_m']/temp_df.loc[:, 'Interval_sec']

			# Further process temp_df to remove rows where speed > max_accepted_speed
			# Extract max accepted speed from df
			max_accept_speed_kmh = max_obs_speed_df.loc[max_obs_speed_df.loc[:, 'Species'] == Unique_sub_df.loc[0, 'individual-taxon-canonical-name'], 'Max_Speed'].iloc[0]
			print ('Species: ' + Unique_sub_df.loc[0, 'individual-taxon-canonical-name'])
			print ('Maximum accepted speed in km/h: ' + str(max_accept_speed_kmh))
			max_accept_speed_ms = max_accept_speed_kmh*1000/60/60
			print ('Maximum accepted speed in m/s: ' + str(max_accept_speed_ms))

			temp_df = temp_df.loc[temp_df.loc[:, 'Speed_ms'] <= max_accept_speed_ms]

			# Add temp data to Distance_df
			Distance_df = Distance_df.append(temp_df, ignore_index = True)


	# Finally, remove rows from Distance_df where Interval_sec is  > 24 hrs
	Distance_df = Distance_df.loc[Distance_df.loc[:, 'Interval_sec'] < 86400]
	# Distance_df = Distance_df.loc[Distance_df.loc[:, 'Interval_sec'] > 0]

	# Create output filepath
	out_fp = out_dat_dir + GPS_file

	# Save Distance_df to csv
	Distance_df.to_csv(out_fp, encoding = 'utf-8')
	
	print ('Finished processing file: ' + GPS_file)



# Set geod to WGS84 ellipsoid
geod = Geodesic.WGS84	

# Establish path to directory containing data to read in
GPS_dat_dir = '../Data/GPS_Data/Raw_Movebank_Data/'

# Establish path to output directory
# Create it if needed
out_dat_dir = '../Data/GPS_Data/Distance_Data/'
if not os.path.exists(out_dat_dir):
	os.makedirs(out_dat_dir)

# List all files in orig_data_directory
file_list = os.listdir(GPS_dat_dir)

# Load max_obs_speed data
max_obs_speed_df = pd.read_csv('../Data/GPS_Data/Max_Speed_Data.csv', sep = ',')


print ('Running GPS_to_Distance.py' + '\n')
# Loop through files in raw_dat_dir and process
for f in file_list:
	data_processing(GPS_file = f)
print ('\n')