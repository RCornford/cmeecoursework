\documentclass[11pt]{article}

\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]
{geometry}
\usepackage{lineno}
\usepackage{setspace}
\usepackage[square, numbers, sort&compress]{natbib}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{float}
\usepackage{adjustbox}


\graphicspath{ {../Results/Report_Figures/} }

\renewcommand{\thetable}{S\arabic{table}}
\renewcommand{\thefigure}{S\arabic{figure}}
\renewcommand{\theequation}{S\arabic{equation}}

%\date{}


\begin{document}
\pagebreak 
\linenumbers
\onehalfspacing

%%%%%%%%%%%%%%%%%%%%
\section*{Supplementary Information}

%%%%%%%%%%%%%%%
\subsection*{Empirical Activity Dataset}

\begin{table}[H]
\caption{\textbf{Summary of the compiled data.} Although mass and $T_p$ are here quoted to two and three decimal places respectively, analysis was conducted on the unrounded values.}
\begin{adjustbox}{width=\textwidth}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{Species} & \textbf{Family} & \textbf{Mass (kg)} & \textbf{$T_p$ Estimate} & \textbf{Diet Type} & \textbf{Sources} \\
\hline
\textit{Myopus schisticolor} & Cricetidae & 0.03 & 0.009 & Selective & \citep{andreassen1991} \\
\hline
\textit{Apodemus flavicolus} & Muridae & 0.03 & 0.030 & Selective & \citep{schwarzenberger1995} \\
\hline
\textit{Dipodomys spectabilis} & Heteromyidae & 0.12 & 0.019 & Selective & \citep{schroder1979} \\
\hline
\textit{Rattus rattus} & Muridae & 0.14 & 0.033 & Selective & \citep{hooker1995} \\
\hline
\textit{Xerus erythropus} & Sciuridae & 0.60 & 0.060 & Selective & \citep{linn1996} \\
\hline
\textit{Lepus americanus} & Leporidae & 1.57 & 0.018 & Mixed & \citep{feierabend2013} \\
\hline
\textit{Lepus europaeus} & Leporidae & 3.82 & 0.016 & Mixed & \citep{schai2012} \\
\hline
\textit{Cephalophus monticola} & Bovidae & 4.88 & 0.026 & Selective & \citep{dubost1980} \\
\hline
\textit{Muntiacus reevesi} & Cervidae & 13.50 & 0.022 & Selective & \citep{chapman1993} \\
\hline
\textit{Cephalophus callipygus} & Bovidae & 18.94 & 0.058 & Selective & \citep{feer1989} \\
\hline
\textit{Cephalophus dorsalis} & Bovidae & 20.00 & 0.042 & Selective & \citep{feer1989} \\
\hline
\textit{Pecari tajacu} & Tayassuidae & 21.22 & 0.067 & Selective & \citep{kays2010data} \\
\hline
\textit{Capreolus capreolus} & Cervidae & 22.45 & 0.064 & Selective & \citep{urbano2014} \\
\hline
\textit{Antidorcas marsupialis} & Bovidae & 33.39 & 0.129 & Mixed & \citep{abrahms2017, abrahms2017data} \\
\hline
\textit{Ovis aries} & Bovidae & 36.65 & 0.107 & Grazer & \citep{steer2012, munn2013} \\
\hline
\textit{Capra hircus} & Bovidae & 47.14 & 0.053 & Grazer & \citep{chynoweth2015, chynoweth2015data}  \\
\hline
\textit{Odocoileus virginianus} & Cervidae & 75.45 & 0.071 & Selective & \citep{webb2010} \\
\hline
\textit{Odocoileus hemionus} & Cervidae & 84.18 & 0.038 & Mixed & \citep{ager2003} \\
\hline
\textit{Tapirus terrestris} & Tapiridae & 167.75 & 0.063 & Selective & \citep{tobler2008} \\
\hline
\textit{Hippotragus niger} & Bovidae & 235.20 & 0.066 & Grazer & \citep{owensmith2010} \\
\hline
\textit{Cervus elaphus} & Cervidae & 240.43 & 0.038 & Mixed & \citep{hebblewhite2008, hebblewhite2008data, nahlik2009, pepin2009, skrobarczyk2011, amos2014, devore2014, mcgeachy2014} \\
\hline
\textit{Equus burchellii} & Equidae & 278.08 & 0.115 & Grazer & \citep{owensmith2010, bartlam2013, bartlam2013data, abrahms2017, abrahms2017data} \\
\hline
\textit{Syncerus caffer} & Bovidae & 592.83 & 0.059 & Grazer & \citep{getz2007, owensmith2010, cross2016data} \\
\hline
\textit{Bison bison} & Bovidae & 622.29 & 0.038 & Grazer & \citep{blake2015data} \\
\hline
\textit{Taurotragus derbianus} & Bovidae & 644.51 & 0.125 & Selective & \citep{bro1997} \\
\hline
\textit{Ceratotherium simum} & Rhinocerotidae & 2272.97 & 0.031 & Grazer & \citep{owensmith1988} \\
\hline
\textit{Elephas maximus} & Elephantidae & 3294.89 & 0.017 & Mixed & \citep{yoganand2015} \\
\hline
\textit{Loxodonta africana} & Elephantidae & 3882.27 & 0.065 & Mixed & \citep{douglas1998, wall2014, wall2014data} \\
\hline
\end{tabular}
\end{adjustbox}
\label{table: Tp_data}
\end{table}

\newpage


%%%%%%%%%%%%%%%
\subsection*{Deriving the Activity Model}
This model was previously derived in \citet{rizzuto2017} and is outlined below.
At the heart of the model is the idea that over the course of a day (24 hours), assuming maintenance only, an organism will balance its energy loss ($E_L$) and energy gain ($E_G$) (in Joules). 
\begin{displaymath}
E_L = E_G
\end{displaymath}
Furthermore, energy loss can be broken down into loss whilst active ($E_A$) and loss whilst resting ($E_B$),
\begin{displaymath}
E_A + E_B = E_G
\end{displaymath} 
whilst the animal's time budget ($T_{tot}$) is also divided into distinct active ($T_A$) and resting ($T_B$) periods.
\begin{displaymath}
T_{tot} = T_A + T_B
\end{displaymath}
By incorporating rates of energy intake ($I$) and loss (whilst active ($A$) and resting ($B$)) (J.s$^{-1}$) we can therefore obtain $E_A = A.T_A$, $E_B = B.T_B$ and $E_I = I.T_A$. Additionally, we can say $E_B = B.(T_{tot} - T_A)$, thus:
\begin{displaymath}
AT_A +  B(T_{tot}-T_A) = IT_A
\end{displaymath}
Rearrangement, and stating that $T_p = T_A/T_{tot}$, yields,
\begin{equation}
T_p = \frac{B}{B - A + I}
\end{equation}
enabling one to predict the proportion of the 24 hour cycle that an animal will be active using knowledge of rates of energy loss and gain. Importantly, under this simplification of reality, where energy costs for growth, reproduction and non-foraging activities are not considered, $T_p$ is more accurately a prediction of the lower limits of an animal's true activity level \citep{rizzuto2017}.
\\
\\
Equation S1 can be further manipulated to allow predictions of $T_p$ based on mass (kg) through the incorporation of metabolic scaling theory. For example:
\begin{equation}
T_p = \frac{B_0 m^{\beta}}{B_0 m^{\beta} - A_0 m^{\alpha} + I_0 m^{i}}
\end{equation}
Where $B_0$, $A_0$ and $I_0$ are the constants of proportionality for the scaling of $B$, $A$ and $I$ respectively with $\beta$, $\alpha$ and $i$ being the associated exponents. 

\newpage

%%%%%%%%%%%%%%%
\subsection*{Parameterising the Activity Model}
In my specific model, the components $B$, $I$ and $A$ had the following structures.
\\
\\
Resting rate of energy loss $B$ was described by a Piecewise Regression model, thus two distinct power equations were used either side of the identified breakpoint (c):
\begin{displaymath}
B_{(m \leq c)} = B_{0,1} m^{\beta_1}, \enspace B_{(m \geq c)} = B_{0,2} m^{\beta_2}
\end{displaymath}
Intake rate $I$ was described by a 2$^{nd}$ Order Polynomial in log$_{10}$ space which can be expressed in non-log$_{10}$ space as:
\begin{displaymath}
I = I_0m^{(i_1 + i_2\cdot log_{10}(m))}
\end{displaymath}
Finally, active rate of energy loss $A$ was of the form:
\begin{displaymath}
A = A_{0,1}m^{\alpha_1}\cdot v + A_{0,2}m^{\alpha_2}
\end{displaymath}
where:
\begin{displaymath}
v = V_{0}m^{p_{v}}
\end{displaymath}

\noindent
The coefficients used to parameterise these equations can be found in Table~\ref{table: Rate_Scaling}. 

%%%%%%%%%%
\begin{table}[H]
\begin{center}
\caption{\textbf{The coefficients and confidence intervals for the empirically derived mass-scaling equations use to parameterise Equation 1.} The number of species (n) included in the analysis and the range of masses covered are also presented with the sources of the data/equations.}
\begin{adjustbox}{width=\textwidth}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Trait} & \textbf{Symbol} & \textbf{Value $\pm$ 95\% CI} & \textbf{Units} & \textbf{n} & \textbf{Mass range (kg)} & \textbf{Source} \\
\hline
\multirow{2}{*}{$B_{(m \leq 1.11kg)}$} & $B_{0,1}$ & 2.43 & \multirow{4}{*}{J.s$^{-1}$} & \multirow{4}{*}{186} & \multirow{4}{*}{0.0083 - 407} & \multirow{4}{*}{\citep{kolokotrones2010}} \\
& $\beta_{1}$ & 0.64 $\pm$ 0.05 & & & & \\
\multirow{2}{*}{$B_{(m \geq 1.11kg)}$} & $B_{0,2}$ & 2.38 & & & & \\
& $\beta_{2}$ & 0.85$ \pm $0.06 & & & & \\
\hline
\multirow{3}{*}{$I$} & $I_0$ & 10.89(9.71, 12.21) & \multirow{3}{*}{J.s$^{-1}$} & \multirow{3}{*}{68} & \multirow{3}{*}{0.03 - 2583} & \multirow{3}{*}{\citep{muller2013}} \\
& $i_{1}$ & 0.66 $\pm$ 0.04 & & & & \\
& $i_{2}$ & 0.05 $\pm$ 0.02 & & & & \\
\hline
\multirow{4}{*}{$A$}  & $A_{0,1}$ & 10.7(10.1, 11.4) & \multirow{4}{*}{J.s$^{-1}$} & \multirow{4}{*}{20}  & \multirow{4}{*}{0.0072 - 254} & \multirow{4}{*}{\citep{taylor1982}} \\
& $\alpha_1$ & 0.684 $\pm$ 0.023 & & & & \\
& $A_{0,2}$ & 6.03(5.6, 6.7) & & & & \\
& $\alpha_2$ & 0.697 $\pm$ 0.042 & & & & \\
\hline
\multirow{2}{*}{$v$} & $V_0$ & 0.33 & \multirow{2}{*}{m.s$^{-1}$} & & & \multirow{2}{*}{\citep{vonbuddenbrock1934}} \\
& $p_v$ & 0.21 & & & & \\
\hline
\end{tabular}
\end{adjustbox}
\label{table: Rate_Scaling}
\end{center}
\end{table}

\newpage

%%%%%%%%%%%%%%%
\subsection*{Additional Analyses}
\subsubsection*{Effects of Seaonality}
\textbf{Methods:}
\\
Seasonality, and the typically associated changes in resource abundance, could influence patterns of activity. To account for this I analysed movement data collected from `Resource Rich' (Wet, Summer, Spring, Autumn) and `Resource Poor' (Dry, Winter) seasons separately. I included Autumn in the `Resource Rich' data as for many herbivores this season provides a bounty of food prior to a harsh Winter. 
\\
\\
As in the main analysis, three models were fitted; Ordinary Least Squares (OLS), a 2$^{nd}$ order polynomial and a piecewise regression, with the best being identified using AICc scores. The presence of a phylogenetic signal in the residuals was also tested for.
\\
\\
\textbf{Results:}
\\
Both the `Rich' and `Poor' datasets were best fitted by the polynomial model (See Figure~\ref{fig: Tp_season}, Table~\ref{table: Tp_season_model} and Table~\ref{table: Tp_season_coeff}), with no evidence of a significant phylogenetic signal (Table~\ref{table: season_lambda}). This suggests that the observed pattern of $T_p$ is consistent across seasons. The hump shaped scaling identified is also qualitatively similar to the tent shape seen in the complete dataset, indicating that the trends identified in the main analysis are representative of a general pattern in nature.

%%%%%%%%%%
\begin{table}[H]
\begin{center}
\caption{\textbf{Summary of model selection criteria for the best fitting models for the seasonal activity datasets.}}
\label{table: Tp_season_model}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Dataset} & \textbf{Best Model} & \textbf{AICc} & \textbf{R$^2$} & \textbf{p value} & \textbf{n} & \textbf{Mass Range (kg)} \\
\hline
Resource Rich & 2$^{nd}$ Order Polynomial & 12.64 & 0.36 & 0.019* & 21 & 0.03 - 3882.27 \\
\hline
Resource Poor & 2$^{nd}$ Order Polynomial & 5.99 & 0.44 & 0.040* & 14 & 1.57 - 3882.27 \\
\hline
\end{tabular}

The p values are calculated for F statistics with (2, n-3) degrees of freedom.
\end{center}
\end{table}

%%%%%%%%%%
\begin{table}[H]
\begin{center}
\caption{\textbf{Coefficients and 95\% Confidence Intervals for the best fitting models for the seasonal activity datasets.}}
\label{table: Tp_season_coeff}
\begin{tabular}{|c|c|c|c|c|}
\hline
\textbf{Dataset} & \textbf{Best Model} & \textbf{a} & \textbf{b} & \textbf{c} \\
\hline
Resource Rich & 2$^{nd}$ Order Polynomial & -1.46 $\pm$ 0.19 & 0.19 $\pm$ 0.13 & -0.05 $\pm$ 0.05 \\
\hline
Resource Poor & 2$^{nd}$ Order Polynomial & -1.83 $\pm$ 0.47 & 0.66 $\pm$ 0.50 & -0.16 $\pm$ 0.12 \\
\hline
\end{tabular}

The 2$^{nd}$ Order Polynomial is of the form $log_{10}(y)$ = a + b$\cdot log_{10}(x)$ + c $\cdot (log_{10}(x))^2$
\end{center}
\end{table}

%%%%%%%%%%
\begin{table}[H]
\begin{center}
\caption{\textbf{Summary of Pagel's $\lambda$ values and associated probability of being zero when testing for phylogenetic signal in the residuals of the best fitted models for the seasonal datasets.} Values for \textbf{n} and \textbf{Mass Range} as in Table~\ref{table: Tp_season_model}. The low $\lambda$ and high p values indicate that there is no significant phylogenetic signal in the model residuals.}
\label{table: season_lambda}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Dataset} & \textbf{Pagel's Lambda ($\lambda$)} & \textbf{p ($\lambda$ = 0)} \\
\hline
Resource Rich & 6.89 $\cdot 10^{-5}$ & 1 \\
\hline
Resource Poor & 8.11 $\cdot 10^{-5}$ & 1 \\
\hline
\end{tabular}
\end{center}
\end{table}

%%%%%%%%%%
\begin{figure}[H]
\begin{center}
\includegraphics[scale = 0.57]{Tp_season}
\caption{\textbf{Observed mass-scaling of $T_p$ in different seasons.} Solid lines represent predicted values of the best fitting models, the shading depicts 95\% confidence intervals, equations of the fitted lines (in log$_{10}$ space) are presented. \textbf{a:} Scaling of $T_p$ in `Resource Rich' conditions. \textbf{b:} Scaling of $T_p$ in `Resource Poor' conditions. Despite the two datasets differing in number of species and mass ranges, the overall pattern is similar in both, suggesting a limited influence of seasonality on $T_p$ scaling in terrestrial, mammalian herbivores.}
\label{fig: Tp_season}
\end{center}
\end{figure}

\newpage

%%%%%%%%%%%%%%%
\subsubsection*{Re-analysis of published data}
\textbf{Methods:}
\\
Both \citet{belovsky1986} and \citet{mysterud1998} report finding statistical relationships between body mass and activity in terrestrial herbivores however neither provide explicit scaling equations. I therefore re-analysed the data in their publications to clarify their findings. Prior to analysis, non-mammalian and captive species were removed from data in \citet{belovsky1986} with number of active minutes being converted to $T_p$. For the \citet{mysterud1998} data, species averages were calculated and percentage of activity was converted to proportion prior to analysis.
\\
\\
To asses the mass-scaling I fitted an OLS regression to log$_{10}$ transformed variables. 
\\
\\
\textbf{Results:}
\\
As indicated in the original studies, the data from \citet{belovsky1986} indicates a positive scaling of activity, with an exponent of 0.11, whilst the data from \citet{mysterud1998} demonstrates a negative scaling and an exponent of -0.12 (see Table~\ref{table: Lit_Tp}).

%%%%%%%%%%
\begin{table}[H]
\begin{center}
\caption{\textbf{Coefficients and 95\% Confidence Intervals for the fitted OLS models.}}
\label{table: Lit_Tp}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Source} & \textbf{a} & \textbf{b} & \textbf{R$^2$} & \textbf{p value} & \textbf{n} & \textbf{Mass range (kg)}\\
\hline
Belovsky and Slade, 1986  & -0.58 $\pm$ 0.10 & 0.11 $\pm$ 0.06 & 0.77 & 0.002** & 9 & 0.035 - 636 \\
\hline
Myterud, 1998 & -0.05 $\pm$ 0.13 & -0.12 $\pm$ 0.07 & 0.44 & 0.003** & 18 & 20 - 331 \\
\hline
\end{tabular}

The OLS model is of the form $log_{10}(y)$ = a + b$\cdot log_{10}(x)$
\\
The p values are given for a t-test testing if the slope of the fitted line equals zero.
\end{center}
\end{table}


%%%%%%%%%%%%%%%
\subsection*{Availability of code}
The code used to conduct this research project can be found in my bitbucket repository: 
\\
https://bitbucket.org/RiCor93/


\newpage
\bibliographystyle{unsrtnat}
\bibliography{Supplementary_Information}
\nocite{*}






\end{document}
