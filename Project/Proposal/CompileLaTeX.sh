#!/bin/bash
# Author: Richard
# Script: CompileLaTeX.sh
# Description: Colmpiles a pdf document from a tex file
#			   Opens the new pdf document


pdflatex rc1015_proposal.tex # Run pdflatex twice to compile the document
pdflatex rc1015_proposal.tex
bibtex rc1015_proposal		# Compiles the biblioraphy into the document
pdflatex rc1015_proposal.tex
pdflatex rc1015_proposal.tex
# evince Writeup.pdf & # Opens the pdf document

## Cleanup
rm *∼
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.bbl
rm *.blg
# Removes extra unnecessary files.
