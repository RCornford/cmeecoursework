# A simple R script to illustrate R input-output.
# Run line by line and check inputs and outputs to understand what is happening

MyData <- read.csv("../Data/trees.csv", header=TRUE) # Import with headers.

write.csv(MyData, "../Results/MyData.csv") # Write it out as a new file.

write.table(MyData[1,], file = "../Results/MyData.csv", append=TRUE) # Appends to the results file, you will get a warning message.

write.csv(MyData, "../Results/MyData.csv", row.names=TRUE) # Writes the row names.

write.table(MyData, "../Results/MyData.csv", col.names=FALSE) # Ignores column names.
