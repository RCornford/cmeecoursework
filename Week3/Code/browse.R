Exponential <- function(N0 = 1, r = 1, generations = 10) {
  # Runs a simulation of exponential growth
  # Returns a vector of length generations
  
  N <- rep(NA, generations) # Creates a vetor of NA
  
  N[1] <- N0
  for (t in 2:generations){
    N[t] <- N[t-1] * exp(r)
#    browser()
  }
  return(N)
}
plot(Exponential(), type="l", main="Exponential growth")
# Plots a graph of the functions output.

# At the point 'browser()' is used, R allows the user to work step-by-step through
# the code, making debugging easier.