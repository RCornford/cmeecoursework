Starting weekly assessment for Richard, Week1

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
PART 1: Checking project workflow...

Found the following directories in parent directory: Week1, Assessment, Week2, .git

Found the following files in parent directory: README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~ 
*.tmp
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2016-17 Coursework Repository
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 2 weekly directories: Week1, Week2

The Week1 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK1...

Found the following directories: Code, Data, Sandbox

Found the following files: ReadMe

Checking for readme file in weekly directory...

Found README in parent directory, named: ReadMe

Printing contents of ReadMe:
**********************************************************************
Description of files in Week1/Code

UnixPrac1.txt : Contains the five single line scripts which are answers for Practical 1 from the silbio handbook. Uses the Fasta files from the Week1/Data/Fasta directory.

boilerplate.sh : This is the first shell script we wrote. When run it produces "This is a shell script!" in the command line.

CountLines.sh : A shell script which counts the number of lines in a file and gives the output "The file '...' has '...' lines". Requires an input file.

ConcatenatTwoFiles.sh : A shell script which combines the content of two files and sends the output to a third file. On the command line, "Merged file is '...'" is printed.

csvtospace.sh : This is a shell script that takes a commma separated file and converts it to a space separated file which is saved in a new file. Use 1800.csv, 1801.csv, 1802.csv and 1803.csv as inputs, these are located in Week1/Data/Temperatures. 

MyExampleScript.sh : A shell script which helps demonstratre how to assign variables and as a consequence prints the message "Hello 'User'". For my computer this prints " Hello cmee07".

tabtocsv.sh : A shell script which converts a tab delimited file into a comma separated file.

varibles.sh : A shell script used to demonstrate how to assign variables.

FirstExample.tex : An example of a very simple LaTeX file.

FirstBiblio.bib : A file contaiing the bibliographic info for the LaTeX document.

CompileLatex.sh : A shell script to automate the production of a .pdf file from a .tex file. For the practical FirstExample.tex was used as the input.

FirstExample.pdf : The compiled pdf version of the LaTeX file.




Description of files in Week1/Data

Fasta : Contains the .fasta files required for UnixPrac1.

Temperatures: Contains the .csv files which act as inputs for the csvtospace.sh script.

spawannxs.txt : A list of protected species. Used when practicing the use of the grep function.



Description of files in Week1/Sandbox

A directory containing files for testing scripts and code on.


**********************************************************************

Results directory missing!

Found 12 code files: ConcatenateTwoFiles.sh, .txt, CountLines.sh, variables.sh, CompileLaTeX.sh, csvtospace.sh, FirstExample.tex, MyExampleScript.sh, FirstBiblio.bib, UnixPrac1.txt, tabtocsv.sh, boilerplate.sh

Found the following extra files: FirstExample.pdf, .csv, .log
What are they doing here?! 

0.5 pt deducted per extra file

Current Marks = 98.5

======================================================================
Testing script/code files...

======================================================================
Inspecting script file ConcatenateTwoFiles.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: ConcatenateTwoFIles.sh
# Desc: merges the contents of two chosen files and outputs this to a third file.
# Arguments: 3 -> two input files and an output file

cat $1 > $3
cat $2 >> $3
echo "Merged File is"
cat $3
**********************************************************************

Testing ConcatenateTwoFiles.sh...

Output (only first 500 characters): 

**********************************************************************
Merged File is

**********************************************************************

Encountered error:
ConcatenateTwoFiles.sh: line 7: $3: ambiguous redirect
ConcatenateTwoFiles.sh: line 8: $3: ambiguous redirect

======================================================================
Inspecting script file .txt...

File contents are:
**********************************************************************
**********************************************************************

Testing .txt...

======================================================================
Inspecting script file CountLines.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: CountLines.sh
# Desc: Counts the number of lines in a file of your choice.
# Arguments: 1 -> a file

NumLines=`wc -l < $1`
echo "The file $1 has $NumLines lines"
echo
**********************************************************************

Testing CountLines.sh...

Output (only first 500 characters): 

**********************************************************************
The file  has  lines


**********************************************************************

Encountered error:
CountLines.sh: line 7: $1: ambiguous redirect

======================================================================
Inspecting script file variables.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: variables.sh
# Desc: Shows the use of variables

MyVar='some string'
echo 'the current value of the variable is' $MyVar
echo 'Please enter a new string'
read MyVar
echo 'the current value of the variable is' $MyVar
## Reading multiple variables
echo 'Enter two numbers separated by space(s)'
read a b
echo 'you entered' $a 'and' $b '. Their sum is:'
mysum=`expr $a + $b`
echo $mysum
**********************************************************************

Testing variables.sh...

Output (only first 500 characters): 

**********************************************************************
the current value of the variable is some string
Please enter a new string
the current value of the variable is
Enter two numbers separated by space(s)
you entered and . Their sum is:


**********************************************************************

Encountered error:
expr: syntax error

======================================================================
Inspecting script file CompileLaTeX.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: CompileLaTeX.sh
# Description: Colmpiles a pdf document from a tex file
#			   Opens the new pdf document
# Argument: tex file without .tex extension
# Date: 10 Oct 2016

pdflatex $1.tex # Run pdflatex twice to compile the document
pdflatex $1.tex
bibtex $1		# Compiles the biblioraphy into the document
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf & # Opens the pdf document

## Cleanup
rm *∼
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.bbl
rm *.blg
# Removes extra unnecessary files.
**********************************************************************

Testing CompileLaTeX.sh...

Output (only first 500 characters): 

**********************************************************************
This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (preloaded format=pdflatex)
 restricted \write18 enabled.
entering extended mode
(/usr/share/texlive/texmf-dist/tex/latex/tools/.tex
LaTeX2e <2016/02/01>
Babel <3.9q> and hyphenation patterns for 3 language(s) loaded.
File ignored)
*
! Emergency stop.
<*> .tex
        
!  ==> Fatal error occurred, no output PDF file produced!
Transcript written on .log.
This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (prel
**********************************************************************

Encountered error:
bibtex: Need exactly one file argument.
Try `bibtex --help' for more information.
rm: cannot remove '*∼': No such file or directory
rm: cannot remove '*.aux': No such file or directory
rm: cannot remove '*.dvi': No such file or directory
rm: cannot remove '*.log': No such file or directory
rm: cannot remove '*.nav': No such file or directory
rm: cannot remove '*.out': No such file or directory
rm: cannot remove '*.snm': No such file or directory
rm: cannot remove '*.toc': No such file or directory
rm: cannot remove '*.bbl': No such file or directory
rm: cannot remove '*.blg': No such file or directory

** (evince:17094): WARNING **: Error when getting information for file '/home/mhasoba/Documents/Teaching/IC_CMEE/2016-17/Assessment/StudentRepos/RichardCornford_RC1015/Week1/Code/.pdf': No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

** (evince:17094): WARNING **: Error setting file metadata: No such file or directory

======================================================================
Inspecting script file csvtospace.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: csvtospace.sh
# Desc: subtitutes the commas for spaces
#		saves output to a new .txt file
# Argument: comma separated file, without .csv extension
# Date: 5 Oct 2016

echo "Creating a space delimited version of $1 ..."

cat $1.csv | tr -s "," " " >> $1.txt # Edits file to substitute commas for spaces. This output is then saved in a new file.

echo "Done!"

exit


**********************************************************************

Testing csvtospace.sh...

Output (only first 500 characters): 

**********************************************************************
Creating a space delimited version of  ...
Done!

**********************************************************************

Code ran without errors

Time consumed = 0.00522s

======================================================================
Inspecting script file FirstExample.tex...

File contents are:
**********************************************************************
\documentclass[12pt]{article}
\title{A Simple Document}
\author{Richard Cornford}
\date{5th Oct 2016}
\begin{document}
\maketitle

\begin{abstract}
This paper must be cool!
\end{abstract}

\section{Introduction}
Blah Blah!

\section{Materials \& Methods}
One of the most famous equations is:
\begin{equation}
E = mc^2
\end{equation}
This equation was first proposed by Einstein in 1905
\cite{einstein1905does}.

\bibliographystyle{plain}
\bibliography{FirstBiblio}
\end{document}
**********************************************************************

Testing FirstExample.tex...

======================================================================
Inspecting script file MyExampleScript.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard
# Script: MyExampleScript.sh
# Desc: Demonstrates how to assign variables.
#		Prints the message "Hello 'user'"

msg1="Hello"
msg2=$USER
echo "msg1 msg2"

echo "Hello $USER"
echo
**********************************************************************

Testing MyExampleScript.sh...

Output (only first 500 characters): 

**********************************************************************
msg1 msg2
Hello mhasoba


**********************************************************************

Code ran without errors

Time consumed = 0.00370s

======================================================================
Inspecting script file FirstBiblio.bib...

File contents are:
**********************************************************************
@article{einstein1905does,
  title={Does the inertia of a body depend upon its energy-content},
  author={Einstein, Albert},
  journal={Annalen der Physik},
  volume={18},
  number={13},
  pages={639--641},
  year={1905}
}
**********************************************************************

Testing FirstBiblio.bib...

======================================================================
Inspecting script file UnixPrac1.txt...

File contents are:
**********************************************************************
#1 Line count for each of the files
wc -l ../Data/Fasta/*.fasta #Uses the * wildcard to open all fasta files in the Data depository. The number of lines in each file is then counted and outputed.


#2 Print E.coli genome
sed '1d' ../Data/Fasta/E.coli.fasta #Prints the E.coli.fasta file minus the first line


#3 Count the length of the E.coli genome
sed '1d' ../Data/Fasta/E.coli.fasta | tr -d "\n" | wc -m #Edits the E.coli.fasta file by removing the first line and the new line characters. The remaining characters are then counted.


#4 Count the number of occurances of 'ATGC' in the E.coli genome
sed '1d' ../Data/Fasta/E.coli.fasta | tr -d "\n" | grep -o ATGC | wc -l #gives 21968 Edits the E.coli.fasta file to remove the first line and the new line characters. The string 'ATGC' is then searced for and outputed with each hit on a new line. The number of lines is then counted.


#5 Calculate the AT/GC ratio in the E.coli genome
echo "scale=10;$(sed '1d' ../Data/Fasta/E.coli.fasta | grep -o -e A -e T | wc -l)/$(sed '1d' ../Data/Fasta/E.coli.fasta | grep -o -e G -e C | wc -l)" | bc
# Edits the E.coli.fasta file to remove the first line. The number of each type of character is counted in a way that generates A+T and G+C as separate values. The AT sum is then divided by the GC sum to give the AT/GC ratio to 10 decimal places.
**********************************************************************

Testing UnixPrac1.txt...

======================================================================
Inspecting script file tabtocsv.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard Cornford rc1015@ic.ac.uk
# Script: tabtocsv.sh
# Desc: Sbstitute the tabs in files with commas.
#		Saves the output into a .csv file.
# Arguments: 1-> tab delimited file
# Date 4 Oct 2016

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit
**********************************************************************

Testing tabtocsv.sh...

Output (only first 500 characters): 

**********************************************************************
Creating a comma delimited version of  ...
Done!

**********************************************************************

Code ran without errors

Time consumed = 0.00604s

======================================================================
Inspecting script file boilerplate.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Richard Cornford rc1015@imperial.ac.uk
# Script: boilerplate.sh
# Desc: simple boilerplate for shell scripts
# Arguments: none
# Date: 4 Oct 2016

echo -e "\nThis is a shell script! \n" 

#exit
**********************************************************************

Testing boilerplate.sh...

Output (only first 500 characters): 

**********************************************************************

This is a shell script! 


**********************************************************************

Code ran without errors

Time consumed = 0.00499s

======================================================================
======================================================================
Finished running scripts

Ran into 4 errors

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 98.5

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!