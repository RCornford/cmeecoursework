Starting weekly assessment for Richard, Week4

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
Your Git repo size this week is about 174.37 MiB on disk 

PART 1: Checking project workflow...

Found the following directories in parent directory: Week1, Assessment, Week2, Week4, .git, Week3

Found the following files in parent directory: README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~ 
*.tmp 
*.bbl 
*.blg 
*.pyc
*.tif
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2016-17 Coursework Repository
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 4 weekly directories: Week1, Week2, Week3, Week4

The Week4 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK4...

Found the following directories: Code, Data

Found the following files: ReadMe

Checking for readme file in weekly directory...

Found README in parent directory, named: ReadMe

Printing contents of ReadMe:
**********************************************************************
Description of files in Week4/Code:

Basic_lm.R : R script demonstraing how to run a basic linear model and generate some statistics from this.

HO10.R : R script created when working through handout 10.

HO13.R : R script created when working through handout 13.

HO14.R : R script created when working through handout 14.

HO16.R : R script created when working through handout 16.



Description of files in Week4/Data:

Daphnia.txt : Ecological data concerning growth rate of Daphnia genotypes under different environmental conditions.

plants.txt : Data on fruit growth, roots size and grazing conditions in plants.

SparrowSize.txt : Ecological survey data on sparrows from lundy Island.

timber.txt : Data on volume, girth and height of trees.


**********************************************************************

Results directory missing!

Found 5 code files: HO10.R, Basic_lm.R, HO13.R, HO14.R, HO16.R

Found the following extra files: .Rhistory
What are they doing here?! 

0.5 pt deducted per extra file

Current Marks = 99.5

======================================================================
Testing script/code files...

======================================================================
Inspecting script file HO10.R...

File contents are:
**********************************************************************
rm(list=ls())

d <- read.table("../Data/SparrowSize.txt", header = T)

plot(d$Mass~d$Tarsus, ylab = "Mass (g)", xlab = "Tarsus (mm", pch = 19, cex = 0.4)

d$Mass[1]
length(d$Mass)
d$Mass[1770]

plot(d$Mass~d$Tarsus, ylab = "Mass (g)", xlab = "Tarsus (mm", pch = 19, cex = 0.4, ylim = c(-5,38), xlim = c(0,22))

d1 <- subset(d, d$Mass!="NA")
d2 <- subset(d1, d1$Tarsus!="NA")

model1 <- lm(Mass~Tarsus, data = d2)
summary(model1)

hist(model1$residuals)

head(model1$residuals)

d2$z.Tarsus <- scale(d2$Tarsus)
model3 <- lm(Mass~z.Tarsus, data = d2)
summary(model3)

plot(d2$Mass~d2$z.Tarsus, pch=19, cex=0.4)
abline(v=0, lty = "dotted")

head(d)
str(d)

d$Sex <- as.numeric(d$Sex)
par(mfrow = c(1, 2))
plot(d$Wing~d$Sex.1, ylab = "Wing (mm)")
plot(d$Wing~d$Sex, xlab = "Sex", xlim = c(-0.1,1.1), ylab = "Wing (mm)")
abline(lm(d$Wing ~d$Sex), lwd = 2)
text(0.15, 76, "intercept")
text(0.9, 77.5, "slope", col = "red")

d4 <- subset(d, d$Wing!="NA")
m4 <- lm(Wing~Sex, data = d4)
t4 <- t.test(d4$Wing~d4$Sex, var.equal = T)
summary(m4)
t4

par(mfrow=c(2,2))
plot(model3)

par(mfrow=c(2,2))
plot(m4)


# Exercises
# 1.
# Test whether sparrow bill size is related to mass
rm(list=ls())
d <- read.table("../Data/SparrowSize.txt", header = T)
mean(d$Bill)
mean(d$Mass)
d1 <- subset(d, d$Bill!="NA")
d2 <- subset(d1, d1$Mass!="NA")
mean(d2$Bill)
mean(d2$Mass)
model1 <- lm(Mass~Bill, data = d2)
summary(model1)
plot(d2$Mass~d2$Bill, xlab = "Bill", ylab = "Mass (g)")
abline(lm(d2$Mass~d2$Bill))

d2males <- subset(d2, d2$Sex == 1)
d2females <- subset(d2, d2$Sex == 0)
length(d2males$Sex)
length(d2$Sex)
model2males <- lm(Mass~Bill, data = d2males)
summary(model2males)
plot(d2males$Mass~d2males$Bill)
model2females <- lm(Mass~Bill, data = d2females)
summary(model2females)
plot(d2females$Mass~d2females$Bill)

**********************************************************************

Testing HO10.R...

Output (only first 500 characters): 

**********************************************************************
[1] 23.6
[1] 1770
[1] 33

Call:
lm(formula = Mass ~ Tarsus, data = d2)

Residuals:
    Min      1Q  Median      3Q     Max 
-7.7271 -1.2202 -0.1302  1.1592  7.5036 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  5.83246    0.98195    5.94 3.48e-09 ***
Tarsus       1.18466    0.05295   22.37  < 2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.841 on 1642 degrees of freedom
Multiple R-squar
**********************************************************************

Code ran without errors

Time consumed = 0.38932s

======================================================================
Inspecting script file Basic_lm.R...

File contents are:
**********************************************************************
x <- c(1, 2, 3, 4, 8)
y <- c(4, 3, 5, 7, 9)

model1<- (lm(y~x))
model1

summary(model1)

anova(model1)

resid(model1)

cov(x, y)

var(x)
var(y)

plot(y~x)

**********************************************************************

Testing Basic_lm.R...

Output (only first 500 characters): 

**********************************************************************

Call:
lm(formula = y ~ x)

Coefficients:
(Intercept)            x  
     2.6164       0.8288  


Call:
lm(formula = y ~ x)

Residuals:
      1       2       3       4       5 
 0.5548 -1.2740 -0.1027  1.0685 -0.2466 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)  
(Intercept)   2.6164     0.8214   3.185   0.0499 *
x             0.8288     0.1894   4.375   0.0221 *
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.
**********************************************************************

Code ran without errors

Time consumed = 0.12463s

======================================================================
Inspecting script file HO13.R...

File contents are:
**********************************************************************
rm(list = ls())

d <- read.table("../Data/SparrowSize.txt", header = T)

d1 <- subset(d, d$Wing!="NA")
summary(d1$Wing)
hist(d1$Wing)

model1 <- lm(Wing~Sex.1, data = d1)
summary(model1)

boxplot(d1$Wing~d1$Sex.1, ylab = "Wing length (mm)")

anova(model1)

t.test(d1$Wing~d1$Sex.1, var.equal = T)


boxplot(d1$Wing~d1$BirdID, ylab = "Wing length (mm)")

library(dplyr)
tbl_df(d1) # Header of table
glimpse(d1) # Summary of table

d$Mass %>% cor.test(d$Tarsus, na.rm = T)

d1 %>% 
  group_by(BirdID) %>% 
  summarise (count=length(BirdID)) %>%
  count(count)

count(d1, BirdID)
# These don't seem to be working, not sure why.

model3 <- lm(Wing~as.factor(BirdID), data = d1)
anova(model3)


boxplot(d$Mass~d$Year)
m2 <- lm(d$Mass~as.factor(d$Year))
anova(m2)
summary(m2)
t(model.matrix(m2))
**********************************************************************

Testing HO13.R...

Output (only first 500 characters): 

**********************************************************************
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   60.0    76.0    77.0    77.4    79.0    84.0 

Call:
lm(formula = Wing ~ Sex.1, data = d1)

Residuals:
     Min       1Q   Median       3Q      Max 
-16.0961  -1.0961  -0.0961   1.3683   5.3683 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept) 76.09611    0.07175 1060.50   <2e-16 ***
Sex.1male    2.53562    0.09998   25.36   <2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘
**********************************************************************

Encountered error:

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union


======================================================================
Inspecting script file HO14.R...

File contents are:
**********************************************************************
rm(list = ls())

d<- read.table("../Data/SparrowSize.txt", header = T)

d1 <- subset(d, d$Wing!="NA")
model3 <- lm(Wing~as.factor(BirdID), data = d1)
anova(model3)

require(dplyr)
d1 %>% 
  group_by(BirdID) %>%
  summarise (count=length(BirdID)) %>%
    summarise (length(BirdID))

d2 <- subset(d, d$Mass!="NA")
d2 %>%
  group_by(BirdID) %>%
  summarise (count = length(BirdID)) %>%
  summarise (sum(count))
# 1704

d2 %>%
  group_by(BirdID) %>%
  summarise (count = length(BirdID)) %>%
  summarise (sum(count^2))
# 7226

# 7226/1704 = 4.24061

d2 %>%
  group_by(BirdID) %>%
  summarise (count = length(BirdID)) %>%
  summarise (length(BirdID))
# 633

# (1/632)*(1704-7226/1704) = 2.689493 = n0

model <- lm(Mass~as.factor(BirdID), data = d2)
anova(model)

((8.8434-1.9177)/2.689493)/(1.9177+(8.8434-1.9177)/2.689493)
# r = 0.573161**********************************************************************

Testing HO14.R...

Output (only first 500 characters): 

**********************************************************************
Analysis of Variance Table

Response: Wing
                    Df Sum Sq Mean Sq F value    Pr(>F)    
as.factor(BirdID)  617 8147.3 13.2047  8.1734 < 2.2e-16 ***
Residuals         1077 1740.0  1.6156                      
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
# A tibble: 1 × 1
  `length(BirdID)`
             <int>
1              618
# A tibble: 1 × 1
  `sum(count)`
         <int>
1         1704
# A tibble: 1 × 1
  `sum(count^2)`
           <dbl
**********************************************************************

Encountered error:
Loading required package: dplyr

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union


======================================================================
Inspecting script file HO16.R...

File contents are:
**********************************************************************
rm(list=ls())

daphnia <- read.delim("../Data/Daphnia.txt")
summary(daphnia)


timber <- read.delim("../Data/timber.txt")
summary(timber)

boxplot(timber$volume)
boxplot(timber$girth)
boxplot(timber$height)


t2 <- as.data.frame(subset(timber, timber$volume!="NA"))
t2$z.girth <- scale(timber$girth)
t2$z.height <- scale(timber$height)

var(t2$z.girth)
var(t2$z.height)
plot(t2)

par(mfrow = c(2,2))
hist(t2$volume)
hist(t2$girth)
hist(t2$height)

pairs(timber)
cor(timber)

t_minus_outlier <- timber[1:30,]

model <- lm(volume ~ girth + height, data = t_minus_outlier)
anova(model)
summary(model)

plants <- read.delim("../Data/plants.txt")
summary(plants)


# 1. Outliers
plot(plants$Root, plants$Fruit)
plot(plants$Grazing, plants$Fruit)


# 2. Variance
plants %>%
  group_by(Grazing) %>%
  summarise (variance=var(Fruit))

# 3. Normal
hist(plants$Fruit)

# 4. 

# 5. Colinearity
pairs(plants)

# 7. Interaction
plot(plants$Grazing, plants$Root)
plot(Fruit~Root, data = plants)
plot(Fruit~Grazing, data = plants)

modelp <- lm(Fruit ~ Root * Grazing, data = plants)
anova(modelp)
summary(modelp)
**********************************************************************

Testing HO16.R...

Output (only first 500 characters): 

**********************************************************************
  Growth.rate     Water     Detergent    Daphnia  
 Min.   :1.762   Tyne:36   BrandA:18   Clone1:24  
 1st Qu.:2.797   Wear:36   BrandB:18   Clone2:24  
 Median :3.788             BrandC:18   Clone3:24  
 Mean   :3.852             BrandD:18              
 3rd Qu.:4.807                                    
 Max.   :6.918                                    
     volume           girth            height    
 Min.   :0.7386   Min.   : 66.23   Min.   :18.9  
 1st Qu.:1.4048   1st Qu.: 88.17   1st Qu.:2
**********************************************************************

Encountered error:
Error: could not find function "%>%"
Execution halted

======================================================================
======================================================================
Finished running scripts

Ran into 3 errors

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 99.5

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!