#!/usr/bin/python

""" A file demonstrating the use of loops in Python."""

# For loops in Python
for i in range(5):
	print i
# Prints 0, 1, 2, 3, 4 with each number on a new line.

my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list:
	print k
# Prints all entries in my_list with each entry on a new line. 

total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands:
	print total + s
# Prints 0, 1, 11, 111, 1111 with each on a new line.

# While loops in Python
z = 0
while z < 100:
	z = z + 1
	print z
# Prints the numbers 1 to 100 with each on a new line.

b = True
while b:
	print "GERONIMO! Infinite loop! ctrl+c to stop!"
# ctrl+c to stop!
# Creates an infinite loop.
