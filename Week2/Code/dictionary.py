#!/usr/bin/python

""" A file containing a tupule called 'taxa', questions set as a practical 
	and the python code answers to these questions.
	"""
	
__author__ = 'Richard Cornford'

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

taxa_dic = {}
# Establishes the empty dictionary 'taxa_dic'

for species in taxa:
	if species[1] not in taxa_dic:
		taxa_dic[species[1]] = set()
	taxa_dic[species[1]].add(species[0])
# Generates a loop through the tupules in taxa.
# If the 'Order' is not present then the 'Order' is added as a key to the dictionary and produces an empty set.
# When the 'Order' is present, the relevant 'Species' is added to the associated set.
print taxa_dic	
# Prints taxa_dic.
