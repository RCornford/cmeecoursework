#!/usr/bin/python

""" A python function for identifying and compiling oak species into a database.
	Also contains a doctest for identyfying issues with the code.
	"""
	
__author__ = 'Richard Cornford'

import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if name starts with 'quercus '
        >>> is_an_oak('quercus')
        True
        
        >>> is_an_oak('quercus robur')
        True
        
        >>> is_an_oak('quercuss robur') 
        False
        
        >>> is_an_oak('Fagus sylvatica')
        False
    """
    return name.lower().startswith('quercus') 
    # The above are control statements devised to test the effectiveness and accuracy of the function 'is_an_oak(name)'.
    # Issue is that 'Quercuss' returns True as it still starts with 'Quercus'.
    # Using "return name.lower() == 'quercus'" eliminates this issue but would then return 
    # False for tests like 'Quercus robur' due to characters after quercus.
    # When using csv files, as below, this may not be an issue as the genus and species names are in separate cells.
    
 
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb')
    g = open('../Data/JustOaksData.csv','wb')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa:
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])    
    
    return 0
# Defines the main function of the module.
# Opens data file with properties 'read, binary' and assigns this to 'f'.
# Opens a results file with properties 'write, binary' and assigns this to 'g'.
# Uses csv.reader to read f and assigns it to the variable taxa.
# Uses csv.writer to write to g.
# Establishes the empty set 'oaks'.
# Sets up a for loop within taxa.
# Prints each row within taxa, equating to the binomial species name.
# Prints "'The genus is' genus".
# If the species belongs to the 'quercus' genus, 
# Quercus is printed,
# 'FOUND AN OAK!' is printed,
# and the species is added to the results file.
# Creates a csv file containing only oak species.

if (__name__ == "__main__"):
    status = main(sys.argv)
# When the module is called, it runs through the main function.

doctest.testmod()
# Enables doc testing when the file is run.
