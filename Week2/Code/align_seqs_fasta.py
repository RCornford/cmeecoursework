#!/usr/bin/python

""" Python code for aligning DNA sequences from two Fasta files with the best alignment and best score being
	outputted to a txt file."""

__author__ = 'Richard Cornford'

import sys

def	align_sequences_fasta(input_file_1, input_file_2):
	""" Takes the Fasta files as inputs, reads them and converts them to sequences without linebreaks, aligns
		the two sequences and generates the best alignment and best socres."""
	f = open(input_file_1, 'rb')
	g = open(input_file_2, 'rb')
	
	line_f = f.readline()
	line_g = g.readline()
	
	sequence_1 = ''
	sequence_2 = ''
	# Opens the specified input files in read format and assigns them to the variables f and g.
	# Reads the input files and asigns them to line_f and line_g.
	# Establishes the empty strings sequences_1 and sequence_2.
	
	while line_f:
		line_f = line_f.rstrip('\n')
		if '>' not in line_f:
			sequence_1 = sequence_1 + line_f
		line_f = f.readline()
	
	while line_g:
		line_g = line_g.rstrip('\n')
		if '>' not in line_g:
			sequence_2 = sequence_2 + line_g
		line_g = g.readline()
	
	# Loops through lines in the read files.
	# The newline character is stripped from each line.
	# Lines which do not contain the > symbol are added to the sequence strings. (Removes first line of sequence info)
	
	#~ print sequence_1
	#~ print sequence_2

	l1 = len(sequence_1)
	l2 = len(sequence_2)
	if l1 >= l2:
		s1 = sequence_1
		s2 = sequence_2
	else:
		s1 = sequence_2
		s2 = sequence_1
		l1, l2 = l2, l1 
	
	# Assigns sequences to s1 and s2 depending on their length, s1 is the longer sequence.
	# l1 and l2 are the lengths of the assigned sequences respectively.
	
	def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
		matched = "" # contains string for alignement
		score = 0
		for i in range(l2):
			if (i + startpoint) < l1:
            # if its matching the character
				if s1[i + startpoint] == s2[i]:
					matched = matched + "*"
					score = score + 1
				else:
					matched = matched + "-"
		# Loop is established to try starting the shorter sequence at all points within the longer sequence.
		# If the aligned bases match, a '*' is generated and 1 is added to the score.
		
    # build some formatted output
		print "." * startpoint + matched           
		print "." * startpoint + s2
		print s1
		print score 
		print ""

		return score
		# Prints matches
		# Prints s2 relative to s1
		# Prints s2
		# Prints score of alignment
		
	calculate_score(s1, s2, l1, l2, 0)
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
	# Establishes a loop to find the best score from the alignment options.
	
	print my_best_align
	print s1
	print "Best score:", my_best_score
	# Prints the best alignment of s2
	# Prints s1
	# Prints "'Best score:' my_best_score"
	
	f = open('../Results/align_seqs_fasta_results.txt', 'wb')
	f.write(my_best_align + '\n' + s1 + '\n' + 'Best score:' + str(my_best_score) + '\n')
	f.close
	# Opens the writable file called 'align_seqs_fasta_result.txt' in a Results depository.
	# Writes the output of the align_seqs_fasta.py script to the txt document.
	# Closes the results document.

def main(argv):
	if len(sys.argv) > 2:
		align_sequences_fasta(sys.argv[1], sys.argv[2])
	else: align_sequences_fasta('../../Week1/Data/Fasta/407228326.fasta', '../../Week1/Data/Fasta/407228412.fasta')
		
if (__name__) == ("__main__"):
	status = main(sys.argv)
	sys.exit
	
# As it is, "run align_seqs_fasta_testing.py" runs the code on the Fasta files defined in main(argv): else.
# "run align_sequences_fasta.py <input_file_1> <input_file_2>" runs the function on the selected files.
# Issue for me is that I have had to define functions within the function in order for the script to run correcly.
# Not sure how best to fix this.
