#!/usr/bin/python

"""Some functions exemplefying the use of control statements"""
# docstrings are considered part of the runnning code, comments are stripped. 
# Therefore you can access you docstrings at run time.

__author__ = 'Richard Cornford (rc1015@ic.ac.uk)'
__version__ = '0.0.1'


import sys 

def even_or_odd(x=0): # If not specified, x should take value 0.
	"""Find whether a number x is even or odd."""
	if x % 2 == 0: # The conditional if
		return "%d is Even!" % x
	return "%d is Odd!" % x
	# If 'x' divides by 2 to give no remainder,
	# 'x is Even!' is returned.
	# If'x' divides by 2 to give a remainder, 
	# 'x is Odd!' is returned.
	
def largest_divisor_five(x=120):
	"""Find which is the largest divisor of x among 2,3,4,5."""
	largest = 0
	if x % 5 == 0:
		largest = 5
	elif x % 4 == 0: # elif means else, if.
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: # When all other conditions are not met
		return "No divisor found for %d!" % x # Each function can return a value or a variable.
	return "The largest divisor of %d is %d" % (x, largest)
	# Establishes a loop to check whether the number given can be divided by 2, 3, 4 and/or 5.
	# If no divisor is found, this is returned.
	# Otherwise the largest divisor of x among 2-5 is returned.
	
def is_prime(x=70):
	"""Find whether an interger is prime."""
	for i in range(2,x): # 'range' returns a sequence of integers.
		if x % i == 0:
			print "%d is not a prime: %d is a divisor" % (x, i) # Print formatted text "%d %s %f %e" % (20, "30", 0.0003, 0.00003)
			return False
	print "%d is a prime!" % x
	return True
	# Establishes a loop dividing x by all integers between 2 and x.
	# If x divides by an integer to leave a remainder of zero, x is found not to be a prime and this info is printed.
	# If x does not divide by an integer to leave a remainder of zero it is a prime and this is printed.
	
def find_all_primes(x=22):
	"""Find all the primes up to x."""
	allprimes = []
	for i in range(2, x + 1):
		if is_prime(i):
			allprimes.append(i)
	print "There are %d primes between 2 and %d" % (len(allprimes), x)
	return allprimes
	# Genertes the empty list 'allprimes'.
	# Establishes a loop to check all integers in range 2 to x+1 (not included).
	# Uses the is_prime function from above to establish if an integer is prime or not.
	# If integer is prime it is added to the allprimes list.
	# The number of primes found is printed.
	# A list of all primes found is printed.
	
def main(argv):
	print even_or_odd(22)
	print even_or_odd(33)
	print largest_divisor_five(120)
	print largest_divisor_five(121)
	print is_prime(60)
	print is_prime(59)
	print find_all_primes(100)
	return 0
	# Defines the main function.
	# Includes some values for x to be used as examples.
	
if (__name__ == "__main__") :
	status = main(sys.argv) 
	sys.exit(status) 
	# If the module is called this causes the main function to be run.
