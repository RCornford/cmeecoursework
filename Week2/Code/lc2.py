#!/usr/bin/python

""" A file containing a rainfall tupule, questions set as a practical 
	and the python codes to answer these questions.
	"""

__author__ = 'Richard Cornford'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

#(1)
print "Part 1:"
months_with_over_100mm_rainfall_plus_rainfall_lc = [monthly_data for monthly_data in rainfall if monthly_data[1]>100]
print "Months and their rainfall when rainfall was greater than 100mm:"
print months_with_over_100mm_rainfall_plus_rainfall_lc
# Calls the month and rainfall data from the rainfall tupule to the list 'months_with_over_100mm_rainfall_plus_rainfall_lc', only if the rainfall in that month was over 100mm.
# Prints the 'months_with_over_100mm_rainfall_plus_rainfall_lc' list.

#(2)
print "\nPart 2:"
months_with_under_50mm_rainfall_lc = [monthly_data[0] for monthly_data in rainfall if monthly_data[1]<50]
print "Months with rainfall less than 50mm:"
print months_with_under_50mm_rainfall_lc
# Calls the month name from the rainfall tupule to the list 'months_with_under_50mm_rainfall', only if the rainfall in that month is under 50mm.
# Prints the 'months_with_under_50mm_rainfall_lc' list.

#(3.1)
#~ months_with_over_100mm_rainfall_plus_rainfall_loop = []
#~ for monthly_data in rainfall:
	#~ if monthly_data[1]>100:
		#~ months_with_over_100mm_rainfall_plus_rainfall_loop.append(monthly_data)
#~ print months_with_over_100mm_rainfall_plus_rainfall_loop
# Establishes an empty list called 'months_with_over_100mm_rainfall_plus_rainfall_loop'.
# Creates a loop that runs through the data in rainfall.
# Adds the month name and rainfall amount to the list if monthly rainfall is over 100mm.
# Prints the 'months_with_over_100mm_rainfall_plus_rainfall_loop' list.

# (3.2)
#~ months_with_under_50mm_rainfall_loop = []
#~ for monthly_data in rainfall:
	#~ if monthly_data[1]<50:
		#~ months_with_under_50mm_rainfall_loop.append(monthly_data[0])
#~ print months_with_under_50mm_rainfall_loop
# Establises an empty list called 'months_with_under_50mm_rainfall_loop'.
# Creates a loop that runs through the data in rainfall.
# Adds the month name to the list if monthly rainfall is under 50mm.
# Prints the 'months_with_under_50mm_rainfall_loop' list.

# Can do part 3 in a single loop:
print "\nPart 3:"

months_with_over_100mm_rainfall_plus_rainfall_loop = []
months_with_under_50mm_rainfall_loop = []

for monthly_data in rainfall:
	if monthly_data[1]>100:
		months_with_over_100mm_rainfall_plus_rainfall_loop.append(monthly_data)
	elif monthly_data[1]<50:
		months_with_under_50mm_rainfall_loop.append(monthly_data[0])

print "Months and their rainfall when rainfall was greater than 100mm:"
print months_with_over_100mm_rainfall_plus_rainfall_loop

print "\nMonths with rainfall less than 50mm:"
print months_with_under_50mm_rainfall_loop
