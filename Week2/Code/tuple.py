#!/usr/bin/python

""" A file containing a birds tupule, questions set for practicals and
	the python codes to answer these quetions.
	"""

__author__ = 'Richard Cornford'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehension!

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

for species_info in birds:
	print "\nLatin name: ", species_info[0]
	print "Common name: ", species_info[1]
	print "Body mass: ", species_info[2]
# Creates a loop and prints each line of the birds tupule on a separate line.

#~ for species_info in birds:
	#~ print (species_info[0] + '\n' + species_info[1] + '\n' + str(species_info[2]) + '\n')
# Creates a loop and prints each part of the tuple of length three on a separate line.

# Wasn't sure which format was desired in question so put two possibilities in.
