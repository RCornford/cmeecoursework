#!/usr/bin/python

""" Reading and writing csv files using python."""

__author__ = 'Richard Cornford'

import csv
# Imports the csv function.

# Read a file containing:
# 'Species', 'Infraorder', 'Family', 'Distribution', Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')
# Opens the testcsv.csv file in read form and assigns it to 'f'.

csvread = csv.reader(f) # Reads the csv file
temp = [] # Creates the empty list 'temp'
for row in csvread: # Loops through the rows in the csv file
	temp.append(tuple(row)) # Appends the temp list with the tupule row.
	print row # Prints the row of info
	print "The species is", row[0] # Prints the species name

f.close

# write a file containing only species name and Body mass
f = open('../Sandbox/testcsv.csv','rb')
g = open('../Sandbox/bodymass.csv','wb')

csvread = csv.reader(f)
csvwrite = csv.writer(g) # Writes the csv file
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]]) # Writes the species name and body mass male to g.

f.close()
g.close()
