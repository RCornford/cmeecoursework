#!/usr/bin/python

""" A file demonstrating the assignemt and usage of global vs local variables."""

__author__ = 'Richard Cornford'

## Try this first

_a_global = 10
# Assigns the value 10 to the variable a_global outside the defined functions.

def a_function():
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	# Defines the function a_function
	# Assigns values to the variables _a_global and _a_local.
	# Prints the statements with the variables. _a_global is 5, _a_local is 4.

a_function()
print "Outside the function, the value is ", _a_global
# Prints the statement and the variable value. _a_global is 10.
# Demonstrates that varables can be assigned different values within a function.

## Now try this

_a_global = 10

def a_function():
	global _a_global #Makes _a_global a constant throughout python
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global
# _a_global is 5, as defined within the function due to using the global command.
# Demonstrates that assigning a global varibale causes it to retain its value both within and outside functions.
