#!/usr/bin/python

""" Some functions demonstrating the use of control statements."""

__author__ = 'Richard Cornford (rc1015@ic.ac.uk)'
__version__ = '0.0.1'


import sys 
import doctest # Import the doctest module

def even_or_odd(x=0): 
	"""Find whether a number x is even or odd.
	
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	Whenever a float is provided, then the closest integer is used:
	
	>>> even_or_odd(3.2)
	'3 is Odd!'
	
	In case of negative numbers the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""
	# The above are control statements to test the function below.
	
	# Define function to be tested
	if x % 2 == 0:
		return "%d is Even!" % x
	return "%d is Odd!" % x
	# If the input number divides by 2 to leave no remainders,
	# 'x is Even!' is returned.
	# If dividing x by 2 leaves a remainder,
	# 'x is Odd!' is returned.

# def main(argv):
	# print even_or_odd(22)
	# print even_or_odd(33)
	# return 0
	
# if (__name__ == "__main__") :
	# status = main(sys.argv) 
# The above is not needed for doctesting.
		
doctest.testmod() # To run with embedded tests.
