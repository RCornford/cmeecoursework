#!/usr/bin/python

""" A module which takes a csv file as an input, aligns the two DNA sequences within the csv file,
	and outputs the best alignment and score in a txt file."""

__author__ = 'Richard Cornford'

import sys
import csv

def align_sequences(input_file):
	""" Opens the csv input file, reads te contained sequences, aligns the sequnces and outpusts the best alignment
		and best score to a txt file."""
	f = open(input_file, 'rb') 
	# Opens the csv file of choice with the two sequences separated by a comma.

	csvread = csv.reader(f)
	temp = []
	for column in csvread:
		temp.append(column)

	f.close

	seq1 = column[0]
	seq2 = column[1]	
	# Reads the csv file
	# Loops through the csv file and puts the sequences into a temp file 
	# Assign the sequences to 'seq1' and 'seq2' based on their column in temp

	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 
	# Assigns the longest sequence to s1, and the shortest to s2
	# l1 is the length of the longest sequence, l2 that of the shortest

	def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
		matched = "" # contains string for alignement
		score = 0
		for i in range(l2):
			if (i + startpoint) < l1:
            # if its matching the character
				if s1[i + startpoint] == s2[i]:
					matched = matched + "*"
					score = score + 1
				else:
					matched = matched + "-"
		# Loop is established to try starting the shorter sequence at all points within the longer sequence.
		# If the aligned bases match, a '*' is generated and 1 is added to the score.
		
    # build some formatted output
		print "." * startpoint + matched           
		print "." * startpoint + s2
		print s1
		print score 
		print ""

		return score
		# Prints matches
		# Prints s1 relative to s2
		# Prints s2
		# Prints score of alignment
		
	calculate_score(s1, s2, l1, l2, 0)
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
	# Establishes a loop to find the best score from the alignment options.
	
	print my_best_align
	print s1
	print "Best score:", my_best_score

	f = open('../Results/align_seqs_result.txt', 'wb')
	f.write(my_best_align + '\n' + s1 + '\n' + 'Best score:' + str(my_best_score) + '\n')
	f.close
	# Opens the writable file called 'align_seqs_result.txt' in a Results depository.
	# Writes the output of the align_seqs.py script to the txt document.
	# Closes the results document.

def main(argv):
	if len(sys.argv) > 1:
		align_sequences(sys.argv[1])
	else: align_sequences(input_file = '../Data/seq.csv')
	
if (__name__) == ("__main__"):
	status = main(sys.argv)
	#~ import sys
	#~ align_sequences(sys.argv[1])
	sys.exit
	
# As it is, typing 'run align_seqs.py <input_file>' into python aligns the input file.
# Typing 'run align_seqs_testing.py' aligns the example file '../Data/seq.csv'.
# Issue for me is that I have had to define functions within the function in order for the script to run correcly.
# Not sure how best to fix this.
