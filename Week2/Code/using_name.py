#!/usr/bin/python
# Filename: using_name.py
""" A file demonstrating the use of modules either as whole entities or as
	indivdual defined functions from within the module.
	"""

__author__ = 'Richard Cornford'

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'

# This is used to allow the module to be used either as a whole (main)
# or it allows for sub-modules to be used separately.
