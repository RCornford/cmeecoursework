#!/usr/bin/python

""" A file demonstrating the function of sys.argv."""

__author__ = 'Richard Cornford'

import sys
print "This is the name of the script", sys.argv[0]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)
# Demonstrates that the first argument in the command line is the name of the script.
# When run without any additional arguments ther is only 1 argument in sys.argv.
# In this case the argument is the name of the file being run.
