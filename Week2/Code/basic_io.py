#!/usr/bin/pyhton

""" An introduction to using loops in python."""

__author__ = 'Richard Cornford'

####
# File Input
####
# Open a file for reading
f = open('../Sandbox/test.txt', 'r')
# Use 'implicit' for loop:
# if the object is a file, python will cycle over the lines
for line in f:
	print line, # the "," prevents adding a new line

# Close the file
f.close

# Same as above but skips printing the blank lines
f = open('../Sandbox/test.txt', 'r')
for line in f:
	if len(line.strip()) > 0: # If a line stripped of spaces is longer than 0 characters
		print line, # Print the line

f.close

####
# File Output
####
# Save the elements of a list to a file
list_to_save = range(100)

f = open('../Sandbox/testout.txt','w')
for i in list_to_save:
	f.write(str(i) + '\n') # Write out string of numbers with each one on a new line

f.close()

####
#Storing objects
####
# To save an object (even complex) for later use
my_dictionary = {"a key": 10, "another key": 11}
# Generates a dictionay called 'my_dictionary'.

import pickle

f = open('../Sandbox/testp.p', 'wb') # note the b: accept binary files
pickle.dump(my_dictionary, f)
f.close()

# load the data again
f = open('../Sandbox/testp.p', 'rb')
another_dictionary = pickle.load(f)
f.close()

print another_dictionary
