#!/usr/bin/python

""" You decribe the program
	here and can use several lines. It is the docstring"""

__author__ = 'Richard Cornford (rc1015@ic.ac.uk)' # Used to provide additional information about code
__version__ = '0.0.1'

# imports
import sys # Module to interface our program with the operating system

# Constants can go here, eg. mathematical constants can be used within the function


# Functions can go here
def main(argv): #argv is the argument, main enables inputs to be specified. When boilerplate is called, python interprets main first. Main can then be used to call other functons.
	print 'This is a boilerplate' 
	return 0 # Useful to have a return value to demonstrate that the programme has run.

if (__name__ == "__main__") : # Makes sure the "main" function is called from commandline
	status = main(sys.argv) # This part processes the arguments and moves them to the 'main'.
	sys.exit(status) # A neat way to end the program
	
# The last two sections allow multiple users to use the python script. Go together in order for function to accept arguments.
