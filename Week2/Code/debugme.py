#!/usr/bin/python

""" A file to demonstrate bugs in Python code and how to use ipdb to identify them."""

__author__ = 'Richard Cornford'

def createbug(x):
	y = x**4
	z = 0.
	import ipdb; ipdb.set_trace() # Imports ipdb and causes the bug trace to occur on the following line of code.
	y = y/z
	return y

createbug(25) # Runs createbug with 25 as the input.
