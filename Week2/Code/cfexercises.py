#!/usr/bin/python

"""A file containing numerous python functions for demonstraing control flows."""

__author__ = 'Richard Cornford'

import sys

# How many times will 'hello' be printed?
# 1)
for i in range(3,17):
	print 'hello'
# Prints 'hello' 14 times. 
# Prints for every integer from 3 (included) to 17 (not included).

# 2)
for j in range(12):
	if j % 3 == 0:
		print 'hello'
# Prints 'hello' 4 times.
# Prints for integers upto but not including 12 that are divisible by 3 without a remainder. Includes 0, 3, 6 and 9.

# 3)
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
# Prints 'hello' 5 times.
# Prints when integers in the range 0 (included) to 15 (not included) divide by 5 to give a remainder of 3 
# or divide by 4 to give a remainder of 3.

# 4)
z = 0
while z != 15:
	print 'hello'
	z = z + 3
# Prints 'hello' 5 times.
# Prints while z is not 15 and after each print, 3 is added to z. prints when z is 0, 3, 6, 9 and 12. 
# Once z becomes 15, the loop is terminated.

# 5)
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1
# Prints 'hello' 8 times.
# Prints 7 times when z is 31 and once when z is 18. Cycles through z = 12 to z= 99.

import sys
# What does fooXX do?


def foo1(x):
	""" Function returning x to the power of 0.5 (square root)."""
	return x ** 0.5


def foo2(x, y):
	""" Function returning the larger of the two numbers entered."""
	if x > y:
		return x
	return y


def foo3(x, y, z):
	"""" Function rearanging the three given numbers based on their relative sizes."""
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z]


def foo4(x):
	""" Function which generates a multiplication series upto but not including i=x+1"""
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result
	# Series pattern: 1 * 1, 1 * 2, 2 * 3, 6 * 4, 24 * 5 etc.
	
# This is a recursive function, meaning that the function calls itself

def foo5(x):
	""" Function which returns x! (x factorial)"""
	if x == 1:
		return 1
	return x * foo5(x - 1)
	# Series patern is: x * x-1 * x-2 * x-3 etc until x =1 
foo5(10)	

"""Defines the main function of the script."""
def main(argv):
	print foo1(5)
	print foo2(8, 13)
	print foo3(3, 10, 17)
	print foo4(8)
	print foo5(6)
	return 0

if (__name__ == '__main__'):
	status = main(sys.argv)
	sys.exit(status)

