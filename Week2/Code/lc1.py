#!/usr/bin/python

""" A file containing a birds tupule, questions set as a practical
	and python scripts to answer these questions. 
	"""

__author__ = 'Richard Cornford'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS


#(1)
# Using list comprehension to generate lists.
print "Part 1:"

latin_names_lc = [species_info[0] for species_info in birds]
print "\nLatin names:"
print latin_names_lc
# Calls the latin names from the birds tupule to the 'latin_names_lc' list.
# Uses a loop established within the list to go through all species.
# Prints the 'latin_names'_lc list.		

common_names_lc = [species_info[1] for species_info in birds]
print "\nCommon names:"
print common_names_lc
# Calls the common names from the birds tupule to the 'common_names_lc' list.
# Uses a loop established within the list to go through all species.
# Prints the 'common_names_lc' list.

mean_body_mass_lc = [species_info[2] for species_info in birds]
print "\nMean body masses:"
print mean_body_mass_lc
# Calls the mean body mass info from the bird tupule to the 'mean_body_mass_lc'.
# Uses a loop established within the list to go through all species.
# Prints the 'mean_body_mass_lc' loop.

#(2)
#Using loops to generate lists.
#~ print "\nPart 2:"
#~ latin_names_loop = []
#~ for species_info in birds:
	#~ latin_names_loop.append(species_info[0])
#~ print "\nLatin names:"
#~ print latin_names_loop
# Creates the empty list 'latin_names_loop'.
# Creates a loop that runs through the species info in the birds tupule.
# Adds the latin names from the tupule to the list 'latin_names_loop'.
# Prints the 'latin_names_loop' list.

#~ common_names_loop = []
#~ for species_info in birds:
	#~ common_names_loop.append(species_info[1])
#~ print "\nCommon names:"
#~ print common_names_loop
# Creates the empty list 'common_names_loop'.
# Creates a loop that runs through the species info in the birds tupule.
# Adds the common names from the tupule to the list 'common_names_loop'.
# Prints the 'common_names_loop' list.

#~ mean_body_mass_loop = []
#~ for species_info in birds:
	#~ mean_body_mass_loop.append(species_info[2])
#~ print "\nMean body masses:"
#~ print mean_body_mass_loop
# Creates the empty list 'mean_body_mass_loop'.
# Creates a loop that runs through the species info in the birds tupule.
# Adds the mean body mass from the tupule to the list 'mean_body_mass_loop'.
# Prints the 'mean_body_mass_loop' list.

# Note can also be done with a single loop.
print "Part 2:"

latin_names_loop = []
common_names_loop = []
mean_body_mass_loop = []

for species_info in birds:
	latin_names_loop.append(species_info[0])
	common_names_loop.append(species_info[1])
	mean_body_mass_loop.append(species_info[2])

print "\nLatin names:"
print latin_names_loop

print "\nCommon names:"
print common_names_loop

print "\nMean body masses:"
print mean_body_mass_loop
