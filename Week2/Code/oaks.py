#!/usr/bin/python

"""" Using loops and list comprehensions to find oak trees 
	 in a list and print them.
	"""

# Finding oak trees from a list of species

taxa = ['Quercus robur',
		'Fraxinus excelsior',
		'Pinus sylvestris',
		'Quercus cerris',
		'Quercus petraea'
		]

def is_an_oak(name):
	""" Function for finding oak trees within a list."""
	return name.lower().startswith('quercus ')
	
# Using for loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species)
print oaks_loops
# Establishes the empty set 'oaks_loops'.
# Loops through the list 'taxa'.
# Uses is_an_oak to identify oaks.
# If the species is an oak it is added to the 'oaks_loops' set.
# The 'oaks_loops' set is printed.

# Using list comprehensions
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc
# Creates the set 'oaks_lc'. Within this it loops through taxa,
# if the species is an oak it is added to the set.
# Prints 'oaks_lc'.

# Get names in UPPER CASE using for loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops
# Same as loop above but species in set are written in upper case.

# Gets name in UPPER CASE using list comprehension
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
# Same as list comprehension above but species in set are written in upper case.
