""" Python code for aligning two genetic sequences 
	so that the alignment generates the highest level of matching.
	"""
__author__ = 'Richard Cornford'

# These are the two sequences to match
# seq2 = "ATCGCCGGATTACGGG"
# seq1 = "CAATTCGGAT"

import csv

f = open('../Data/seq.csv', 'rb') 
# Opens the csv file with the two above sequences separated by a comma

csvread = csv.reader(f)
temp = []
for column in csvread:
	temp.append(column)

f.close

seq1 = column[0]
seq2 = column[1]	
# Reads the csv file
# Loops through the csv file and puts the sequences into a temp file 
# Assign the sequences to 'seq1' and 'seq2' from their column in temp

# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # build some formatted output
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print score 
    print ""

    return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

print my_best_align
print s1
print "Best score:", my_best_score

f = open('../Results/align_seqs_result.txt', 'wb')
f.write(my_best_align + '\n' + s1 + '\n' + 'Best score:' + str(my_best_score) + '\n')
f.close
# Opens the writable file called 'align_seqs_result.txt' in a Results depository.
# Writes the output of the align_seqs.py script to the txt document.
# CLoses the results document.
