import csv

f =open('../Sandbox/fasta.csv', 'r')

csvread = csv.reader(f)
temp = []
for row in csvread:
	temp.append(row)
# print temp
f.close

seq1 = row[0]
seq2 = row[1]

print seq1
print seq2
# Biopython!!!
# Open fasta files
# Extract genome codes
# Remove first line and \n characters
# Assign seq to s1 and s2

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # build some formatted output
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print score 
    print ""

    return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

print my_best_align
print s1
print "Best score:", my_best_score

f = open('../Results/align_seqs_fasta_results.txt', 'wab')
f.write(my_best_align + '\n' + s1 + '\n' + 'Best score:' + str(my_best_score) + '\n')
f.close
