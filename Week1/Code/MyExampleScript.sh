#!/bin/bash
# Author: Richard
# Script: MyExampleScript.sh
# Desc: Demonstrates how to assign variables.
#		Prints the message "Hello 'user'"

msg1="Hello"
msg2=$USER
echo "msg1 msg2"

echo "Hello $USER"
echo
