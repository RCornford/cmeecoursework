#!/bin/bash
# Author: Richard Cornford rc1015@ic.ac.uk
# Script: tabtocsv.sh
# Desc: Sbstitute the tabs in files with commas.
#		Saves the output into a .csv file.
# Arguments: 1-> tab delimited file
# Date 4 Oct 2016

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit
