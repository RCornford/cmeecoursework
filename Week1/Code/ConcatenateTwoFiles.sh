#!/bin/bash
# Author: Richard
# Script: ConcatenateTwoFIles.sh
# Desc: merges the contents of two chosen files and outputs this to a third file.
# Arguments: 3 -> two input files and an output file

cat $1 > #../Sandbox/Concat_Output.txt #$3
cat $2 >> #../Sandbox/Concat_Output.txt #$3
echo "Merged File is"
cat ../Sandbox/Concat_Output.txt #$3
