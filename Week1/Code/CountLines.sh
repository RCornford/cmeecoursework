#!/bin/bash
# Author: Richard
# Script: CountLines.sh
# Desc: Counts the number of lines in a file of your choice.
# Arguments: 1 -> a file

NumLines=`wc -l < $1`
echo "The file $1 has $NumLines lines"
echo
