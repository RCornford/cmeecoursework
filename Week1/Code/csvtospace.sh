#!/bin/bash
# Author: Richard
# Script: csvtospace.sh
# Desc: subtitutes the commas for spaces
#		saves output to a new .txt file
# Argument: comma separated file, without .csv extension
# Date: 5 Oct 2016

echo "Creating a space delimited version of $1 ..."

cat $1.csv | tr -s "," " " >> $1.txt # Edits file to substitute commas for spaces. This output is then saved in a new file.

echo "Done!"

exit


