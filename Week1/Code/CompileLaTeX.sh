#!/bin/bash
# Author: Richard
# Script: CompileLaTeX.sh
# Description: Colmpiles a pdf document from a tex file
#			   Opens the new pdf document
# Argument: tex file without .tex extension
# Date: 10 Oct 2016

pdflatex $1.tex # Run pdflatex twice to compile the document
pdflatex $1.tex
bibtex $1		# Compiles the biblioraphy into the document
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf & # Opens the pdf document alternative output? (../Results/$1.pdf)

## Cleanup
rm *∼
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
# Removes extra unnecessary files.
