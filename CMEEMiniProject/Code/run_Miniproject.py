#!/usr/bin/python

"""Python script for running the miniproject workflow."""

__author__ = 'Richard Cornford'

# Imports the subprocess module
import subprocess

# Runs the Data_Maniupulation.R script in R
subprocess.os.system("Rscript Data_Manipulation.R")

# Runs the NLLS.py script in python
subprocess.os.system("python NLLS.py")

# Runs the Analysis.R script in R
subprocess.os.system("Rscript Analysis.R")

# Runs the compile LateX script in Bash
subprocess.os.chdir("../Report")
subprocess.os.system("bash CompileLaTeX.sh")

