#!/bin/bash
# Author: Richard
# Script: CompileLaTeX.sh
# Description: Colmpiles a pdf document from a tex file
#			   Opens the new pdf document


pdflatex Writeup.tex # Run pdflatex twice to compile the document
pdflatex Writeup.tex
bibtex Writeup		# Compiles the biblioraphy into the document
pdflatex Writeup.tex
pdflatex Writeup.tex
# evince Writeup.pdf & # Opens the pdf document

## Cleanup
rm *∼
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.bbl
rm *.blg
rm IC_Crest-eps-converted-to.pdf
# Removes extra unnecessary files.
