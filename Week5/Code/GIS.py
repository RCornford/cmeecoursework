#!/usr/bin/python

""" Python script for completing the assigned pracical.
	Merges, clips and reprojects bio_clim data.
	Clips and reprojects landcover data.
	Extracts bio_clim data for a given landcover class and calculates the average.
	Outputs results to zonalstats.csv.
"""

# Import the required modules.
import numpy 
import gdal  
import os
import csv

# Establish bio_clim variables.
bio_clim = ["bio1", "bio12"]

# Merge bio_clim variables, clip bio_clim variables to cover UK, reproject bio_clim variables to British National Grid.
for clim_var in bio_clim:
	
	command = "gdal_merge.py -n -32768 -a_nodata -32768 -of GTiff -o ../Data/{f}_merge.tif ../Data/{f}_15.tif ../Data/{f}_16.tif"
	os.system(command.format(f = clim_var))
	
	command = "gdal_translate -projwin -12.0 60.0 4.0 48.0 -of GTiff ../Data/{f}_merge.tif ../Data/{f}_UK.tif"
	os.system(command.format(f = clim_var))
	
	command = "gdalwarp -overwrite -s_srs EPSG:4326 -t_srs EPSG:27700 -r cubic -dstnodata -32768 -of GTiff ../Data/{f}_UK.tif ../Data/{f}_UK_BNG.tif -tr 2000 2000 -te -220000 -10000 680000 1080000"
	os.system(command.format(f = clim_var))

# Clip lancover data to cover UK.
command = "gdal_translate -projwin 2800000.0 4200000.0 4000000.0 3000000.0 -of GTiff ../Data/g250_06.tif ../Data/g250_06_UK.tif"
os.system(command)

# Reproject landcover data to British National Grid.
command = "gdalwarp -overwrite -s_srs EPSG:3035 -t_srs EPSG:27700 -r near -of GTiff ../Data/g250_06_UK.tif ../Data/g250_06_UK_BNG.tif -tr 2000 2000 -te -220000 -10000 680000 1080000"
os.system(command)

# Remove unneccessary intermediate files.
intermediates = ['../Data/g250_06_UK.tif', '../Data/bio1_merge.tif', '../Data/bio1_UK.tif',
                 '../Data/bio12_merge.tif', '../Data/bio12_UK.tif']
for x in intermediates:
	os.remove(x)


def zonalStats(layer, classMap, classCodes):
    """ Loads a bio_clim layer and runs the meanExtract function
    within each landcover class.
    """
    # Loads the bio_clim data into an array.
    bioclimLayer = gdal.Open('../Data/{f}_UK_BNG.tif'.format(f=layer))
    bcData = bioclimLayer.ReadAsArray()

    # Finds and masks the no data values in the bio_clim data.
    bioclimBand = bioclimLayer.GetRasterBand(1)
    noData = bioclimBand.GetNoDataValue()
    bcData = numpy.ma.masked_where(bcData == noData, bcData)

    # Runs the mean extraction function on bcData.
    means = [meanExtract(lcc, classMap, bcData) for lcc in classCodes]

    # Closes the layer.
    bioclimLayer = None

    return means


def meanExtract(lcc, classMap, data):
    """ Extracts the values from the bio_clim layer for a given landcover class.
    Then calculates the mean of these to 2dp. 
    Assumes no data values are already masked.
    """
    # Creates a paired list of indices in the landcover that
    # match the input value.
    locs = numpy.where(classMap == lcc)

    # Extracts those indices from the bioclim data.
    vals = data[locs]
	
	# Returns the means, rounded to 2dp.
    return round(vals.mean(), 2)

# Loads the landcover data into an array.
landcover = gdal.Open('../Data/g250_06_UK_BNG.tif')
lcData = landcover.ReadAsArray()

# Finds the set of landcover classes.
lcClasses, lcFreq = numpy.unique(lcData, return_counts=True)

# Gets a list of values for each variable.
zonalMeans = [zonalStats(x, lcData, lcClasses) for x in bio_clim]

# Creates a list of  columns to export (LCC codes, bio1 mean, bio12 mean).
columns = [list(lcClasses)] + zonalMeans
# Unpacks the data into rows.
rows = zip(*columns)

# Writes the rows to a csv file.
with open('../Results/zonalstats.csv', 'wb') as csvfile:
    c = csv.writer(csvfile)
    c.writerow(['LCC', 'bio1', 'bio12'])
    for x in rows:
        c.writerow(list(x))

# Creates a list of generated files needed for zonalstats.
generated_files = ["../Data/bio1_UK_BNG.tif", "../Data/bio12_UK_BNG.tif", 
				   "../Data/g250_06_UK_BNG.tif"]
# Removes these files to keep data directory clean.				   
for y in generated_files:
	os.remove(y)
