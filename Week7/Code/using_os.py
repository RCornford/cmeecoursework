#!/usr/bin/python

""" Pyton script for finding files and directories."""

# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
	for f in files:
		if f.startswith("C"):
			FilesDirsStartingWithC.append(f)
	for s in subdir:
		if s.startswith("C"):
			FilesDirsStartingWithC.append(s)

print "Files and Directories starting with C are: ", "\n", FilesDirsStartingWithC 
print "Number of Files and Directrories Starting With C is: ", len(FilesDirsStartingWithC), "\n"

# list comprehencsion version	
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:
FilesDirsStartingWithCc = []

for (dir, subdir, files) in subprocess.os.walk(home):
	for f in files:
		if f.startswith(("C", "c")):
			FilesDirsStartingWithCc.append(f)
	for s in subdir:
		if s.startswith(("C", "c")):
			FilesDirsStartingWithCc.append(s)

print "Files ancd Directories starting with C or C are: ", "\n",  FilesDirsStartingWithCc 
print "Number of Files and Directrories Starting With C or c is: ", len(FilesDirsStartingWithCc), "\n"

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:
DirsStartingWithCc = []

for (dir, subdir, files) in subprocess.os.walk(home):
	for s in subdir:
		if s.startswith(("C", "c")):
			DirsStartingWithCc.append(s)

print "Directories starting with C or C are: ", "\n", DirsStartingWithCc  
print "Number of Directrories Starting With C or c is: ", len(DirsStartingWithCc), "\n"
