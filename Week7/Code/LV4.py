#!/usr/bin/python

""" Python script for generating the discrete time LV model,
	with random fluctuation in resource growth rate at each timestep."""

""" Python script for generating the discrete time LV model."""
import scipy as sc
import scipy.stats as stats
import pylab as p


# Assign values to parameters:
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficiency
K = 40 # Resource carrying capacity
t = 100 # Time span
R = 10 # Initial pop size of resource
C = 5 # Initial pop size of consumer

# Create an array to hold population data
pop = sc.array([[0,R,C]])

# Create a loop to run calculations through time.
#~ for i in range(0,t+1):
	#~ print R
	#~ print C
	#~ E = float(stats.norm.rvs(size = 1))
	#~ R1 = R*(1+(r+E)*(1-R/K)-a*C)
	#~ C1 = C*(1-z+e*a*R)
	#~ pop = sc.append(pop, [[R1, C1]], axis = 0)
	#~ if R1 < 0:
		#~ break
	#~ elif C1 < 0:
		#~ break
	#~ else:
		#~ R = R1
		#~ C = C1

# Create a loop to run calculations through time.
for i in range(t):
	# Creates the random error term from a normal distribution.
	ER = float(stats.norm.rvs(size = 1))
	# Equations for calculating population sizes in the next generation. Uses the pop array to get input values based on time i.
	R1 = (pop[i,1])*(1+(r+ER)*(1-(pop[i,1])/K)-a*(pop[i,2]))
	C1 = (pop[i,2])*(1-z+e*a*(pop[i,1]))
	# Appends pop array with populations generated in each loop.
	pop = sc.append(pop, [[i+1, R1, C1]], axis = 0)
	# If either of the populations go extinct, the loop stops.
	if R1 < 0 or C1 < 0:
		break

#~ print pop

# Transposes the info in the pop array to x, prey, and predators.
x, prey, predators = pop.T
f1 = p.figure() #Open empty figure object
p.plot(x, prey, 'g-', label='Resource density') # Plot
p.plot(x, predators, 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.text(((len(pop))/2), min(prey),"r = %r, a = %r, z = %r, e = %r, K = %r" % (r, a, z, e, K), horizontalalignment='center', verticalalignment='bottom')
# p.show()
f1.savefig('../Results/LV4_plot.pdf')
