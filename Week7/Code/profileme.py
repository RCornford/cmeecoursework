#!/usr/bin/python

""" Python script demonstrating the use of profiling for efficient codes."""


def a_useless_function(x):
	"""Function running through values 0 to 99999999."""
	y = 0 
	for i in xrange(100000000):
		y = y + i
	return 0


def a_less_useless_function(x):
	"""Function running through values 0 to 99999."""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0


def some_function(x):
	"""Function which prints the argument supplied and runs the above functions."""
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)
