#!/usr/bin/python

""" Python script demonstrating how to use timeit in order to identify quicker ways of computing the same thing."""

# Import timeit
import time

# Comparing the speeds of range and xrange.

def a_not_useful_function():
	"""Function that runs through values 0 to 99999 using range."""
	y = 0
	for i in range(100000):
		y = y + i
	return 0


def a_less_useless_function():
	"""Function that runs through values 0 to 99999 using xrange."""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0

# One approach to time it is:
start = time.time()
a_not_useful_function()
print "a_not_useful_function takes %f s to run." % (time.time() - start)

start = time.time()
a_less_useless_function()
print "a_less_useless_function takes %f s to run." % (time.time() - start)


#Comparing the speeds of loops and list comprehensions.

my_list = range(1000)


def my_squares_loop(x):
	"""Function that outputs the squares of values in range x using loop."""
	out = []
	for i in x:
		out.append(i ** 2)
	return out


def my_squares_lc(x):
	"""Function that outputs the squares of values in range x using list comprehension."""
	out = [i ** 2 for i in x]
	return out

# %timeit(my_squares_loop(my_list))
# %timeit(my_squares_lc(my_list))


# Comparing the speeds of the loops and join method.

import string

my_letters = list(string.ascii_lowercase)


def my_join_loop(l):
	"""Function adding letters to a string using loop."""
	out = ''
	for letter in l:
		out += letter
	return out


def my_join_method(l):
	"""Function adding letters to a string using join."""
	out = ''.join(l)
	return out

# %timeit(my_join_loop(my_letters))
# %timeit(my_join_method(my_letters))

# Oh dear


def getting_silly_pi():
	"""Function that runs through values 0 to 99999."""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0


def getting_silly_pii():
	"""Function that runs through values 0 to 99999 using +=."""
	y = 0
	for i in xrange(100000):
		y += i
	return 0

# %timeit(getting_silly_pi())
# %timeit(getting_silly_pii())
