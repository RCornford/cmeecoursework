#!/usr/bin/python

""" Python script for running LV scripts as needed for the practical.
	Also runs the extra-credit scripts."""

# 1. Running LV1 and LV2 	
# Runs LV1 using the execfile command
#execfile("LV1.py")

# Imports the subprocess package
import subprocess
# Runs LV1 using subprocess.os.system
# subprocess.os.system("python LV1.py")
# Runs LV2 with arguments using the subprocess.os.system command
# subprocess.os.system("python LV2.py 1. 0.1 1.5 0.75 150")


# 2. Profiling LV1 and LV2
# Imports package cProfile
import cProfile

# Profiles LV1 using cProfile
#~ subprocess.os.system("python -m cProfile -s tottime LV1.py > ../Results/LV1_Profiling.txt &")
print "Profiling LV1.py:"
subprocess.os.system("python -m cProfile LV1.py")

#Profiles LV2 using cProfile
#~ subprocess.os.system("python -m cProfile -s tottime LV2.py 1. 0.1 1.5 0.75 150 > ../Results/LV2_Profiling.txt")
print "Profiling LV2.py:"
subprocess.os.system("python -m cProfile LV2.py 1. 0.1 1.5 0.75 150")

# 3. Extra-credit: Final population values are printed when LV2.py runs.
subprocess.os.system("python LV2.py 1. 0.1 1.5 0.75 150")

# 4. Extra-extra-credit: Discrete time model, run/profile LV3.py
#~ subprocess.os.system("python -m cProfile -s tottime LV3.py > ../Results/LV3_Profiling.txt")
print "Profiling LV3.py:"
subprocess.os.system("python -m cProfile LV3.py")

# 5. Extra-extra-extra-credit: Discrete time model + stoch in resource growth, run LV4.py
#~ subprocess.os.system("python -m cProfile -s tottime LV4.py > ../Results/LV4_Profiling.txt")
print "Profiling LV4.py:"
subprocess.os.system("python -m cProfile LV4.py")

# 6. Extra-extra-extra-credit: Discrete time model + stoch in resource growth and consumer growth, run LV5.py
print "Profiling LV5.py:"
subprocess.os.system("python -m cProfile LV5.py")
