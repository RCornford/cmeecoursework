#!/usr/bin/python

"""Coursework submission. 
Python code for finding and extracting Kingdom, Phylum and Species data
in blackbirds.txt.
"""

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = str(text.decode('ascii', 'ignore'))
# Also converts to string for using regex.

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# my_reg = ??????

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 

# Finds all occurances of 'Kingdom' followed by a single word in 'text'.
#kingdoms_match = re.findall(r'Kingdom\s+\w+', text)
# Finds all occurances of 'Phylum' followed by a single word in 'text'.
#phyla_match = re.findall(r'Phylum\s+\w+', text)
# Finds all occurances of 'Species' followed by two words in 'text'.
#species_match = re.findall(r'Species\s+\w+\s+\w+', text)

#print "Taxonomy Summary:" + "\n"

# Loops through 'kingdoms_match'.
# For each loop prints the taxonomic data for a particular species.
#for i in range(len(kingdoms_match)):
#	print "Species " + str(i+1) + "\n" + kingdoms_match[i] + "\n" + phyla_match[i] + "\n" + species_match[i] + "\n"

# Finds all occurances of an alphanumeric sequence following 'Kingdom ' in text.
kingdoms = re.findall(r'(?<=Kingdom\s)\w+', text)
# Finds all occurances of an alphanumeric sequence following 'Phylum ' in text.
phyla = re.findall(r'(?<=Phylum\s)\w+', text)
# Finds all occurances of two alphanumeric sequences following 'Species ' in text.
species = re.findall(r'(?<=Species\s)\w+\s+\w+', text)

# Creates a tupule of the taxonomic information, grouped by the species.
taxonomy = zip(kingdoms, phyla, species)

print "\n" + "Taxonomy Summary" + "\n"

# Loops through the groups in 'taxonomy', printing the relevant info.
for i in range(len(taxonomy)):
	print ("Species " + str(i+1) + " Information" + "\n" +	
	"Kingdom: " + taxonomy[i][0] + "\n" +	
	"Phylum: " + taxonomy[i][1] + "\n" +	
	"Species: " + taxonomy[i][2] + "\n")

#~ for i in range(len(kingdoms)):
	#~ print ("Species " + str(i+1) + " Information" + "\n" +	
	#~ "Kingdom: " + kingdoms[i] + "\n" +	
	#~ "Phylum: " + phyla[i] + "\n" +	
	#~ "Species: " + species[i] + "\n")
