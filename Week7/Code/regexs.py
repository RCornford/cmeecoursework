#!/usr/bin/python

""" A file demonstrating the use of regex in python."""

__author__ = 'Richard Cornford'

my_string = "a given string"

# Find a space in the string
match = re.search (r'\s', my_string)

print match
# This should print something like
# <_sre.SRE_Match object at 0x93ecdd30>

# Now we can see what it has matched
match.group()


match = re.search(r's\w*', my_string)

# This should return "string"
match.group()


# Now an example of no match:
# Find a digit in the string
match = re.search(r'\d', my_string)

# This should print "None"
print match


# Further eg.
my_string = 'an example'
match = re.search(r'\w*\s', my_string)

if match:
	print 'Found a match:', match.group()
else:
	print 'Did not find a match.'


MyStr = 'an example'

match = re.search(r'\w*\s', MyStr)

if match:
	print 'Found a match:', match.group()
else:
	print 'Did not find a match.'
	
	
# Some more basic examples
match = re.search(r'\d' , "it takes 2 to tango")
print match.group() # This should print "2"

match = re.search(r'\s\w*\s', 'once upon a time')
match.group() # Should print " upon "

match = re.search(r'\s\w{1,3}\s', 'once upon a time')
match.group() # Should return " a "

match = re.search(r'\s\w*$', 'once upon a time')
match.group() # Should return ' time'

match = re.search(r'^\w*\s\d.*\d' , 'take 2 grams of H2O')
match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', 'once upon a time')
match.group() # 'once upon a '


# Note: *, + and {} are "greedy"
# They repeat the previous regex token as many times as possble
# They therefore amtch more text than you want

# To make it non-greedy, use "?"
match = re.search(r'^\w*.*?\s', 'once upon a time')
match.group() # 'once '

# Further demonstration of greediness, matching an HTML tag
match = re.search(r'<.+>', This is a <EM>first</EM> test')
match.group() # Returns '<EM>first</EM>' but we only wanted '<EM>', this is because '+' is greedy!

# Make + lazy using ?
match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group() # '<EM>'

# Moving on from greed and laziness
match = re.search(r'\d*\.?\d*', '1432.75+60.22i') # Note the \ before .
match.group() # '1432.75'

match = re.search(r'\d*\.?\d*', '1432+6022i')
match.group() #'1432'

match = re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() # 'ATTGCT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog''s name is Theloderma asper').group() # ' Theloderma asper'
# Note that you can directly return the result by appending .group






