#!/usr/bin/python

""" Python script for running an R code which outputs a graph.
	Success of run is also indicated."""
	
import subprocess
import os.path

# Use subprocess.Popen to run fmr.R using the bash shell.
subprocess.Popen("Rscript fmr.R", shell = True).wait()

# Conditional statements for determining success of running the code.
if os.path.isfile("../Results/fmr_plot.pdf"):
	print "Run successful: fmr_plot.pdf printed."
else:
	print "Run unsuccessful."

if os.path.isfile("../Results/sorted_species.csv"):
	print "Run successful: sorted_species.csv written."
else:
	print "Run unsuccessful."
	
#~ --verbose
