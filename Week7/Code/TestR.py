#!usr/bin/python

""" Python script for runnig TestR.R.
	Outputs are printed in assigned files."""

import subprocess

# Runs Rscript on TestR.R, as Rscript is in usr/bin/ it is directly accessible from the bash terminal.
# Outputs result of TestR.R to TestR.Rout
subprocess.Popen("Rscript --verbose TestR.R > ../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout", shell=True).wait()
