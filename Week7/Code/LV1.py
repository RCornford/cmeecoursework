#!/usr/bin/python

""" The typical Lotka-Volterra Model simulated using scipy """

# Imports packages and assigns names. 
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

# Define function, contains equations for Lotka-Volterra.
def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    # Establishes R and C as values from the pops array.
    R = pops[0]
    C = pops[1]
    # Gives the equations for calculating dRdt and dCdt
    dRdt = r*R - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    #Returns the array contining all values of dRdt and dCdt. 
    return sc.array([dRdt, dCdt])

# Define parameters:
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficiency

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000)

# Determins initial conditions: 10 prey and 5 predators per unit area
x0 = 10
y0 = 5 
# Input these initial conditions into the array z0.
z0 = sc.array([x0, y0]) 

# Runs integration using the ordinary differential equation function.
# Uses the dCR_dt function as an input, z0 as initial conditions and t as time span.
# Outputs results of integration to pops.
pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

# Transposes the pops array to assign values for prey and predators. (first column = prey, second = predators)
prey, predators = pops.T # Transpose the output.
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plots prey against time, green line, labelled.
p.plot(t, predators  , 'b-', label='Consumer density') # Plots predators against time, blue line, labelled.
p.grid() # Plots a grid on the graph
p.legend(loc='best') # Plots a legend and positions it in the best place.
p.xlabel('Time') # Labels the x axis 'ime'
p.ylabel('Population') # Labels the y axis 'Population'
p.title('Consumer-Resource population dynamics') # Gives the plot a title
#p.show()
f1.savefig('../Results/prey_and_predators_1.pdf') #Saves figure
