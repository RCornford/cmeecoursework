#!/usr/bin/python

""" Python script for making and manipulating a database in sqlite."""

# import the SQLite3 library
import sqlite3

# Create connection to the databese
# Creates the database test.db
conn = sqlite3.connect("../Data/test.db")

# To execute command create a "cursor"
c = conn.cursor()

# Use the cursor to execute the queries
# Use the triple single quotes to write queries on several lines
# Creates the table Test with fields ID, MyVal1 and MyVal2.
c.execute('''CREATE TABLE Test 
			(ID INTEGER PRIMARY KEY,
			MyVal1 INTEGER,
			MyVal2 TEXT)''')
			
# Insert the records. 
# Because we set the primary key it will auto-increment. Therefore set to NULL.
c.execute('''INSERT INTO Test VALUES
			(NULL, 3, 'mickey')''')

c.execute('''INSERT INTO Test VALUES
			(NULL, 4, 'mouse')''')
			
# Commiting causes all commands to be executed
conn.commit()

# Select the records
c.execute("SELECT * FROM TEST")

# Access the next record:
print c.fetchone()
print c.fetchone()

# To get all the records at once
c.execute("SELECT * FROM TEST")
print c.fetchall()

# Insert many records at once:
# Create a list of tuplesTest
manyrecs = [(5, 'goofy'),
(6, 'donald'),
(7, 'duck')]

# Call executemany
c.executemany('''INSERT INTO test
VALUES(NULL, ?, ?)''', manyrecs)

# Commit
conn.commit()

# Fetch the records
# we can use the query as an iterator!
for row in c.execute('SELECT * FROM test'):
	print 'Val', row[1], 'Name', row[2]

# close the connection before exiting
conn.close()
