#!/usr/bin/python

"""Regular expression for finding relaistic dates.
"""

# Imports the rgular expressions function.
import re

# Establishes the string 'dates'.
dates = "19930407"
# 20010712, 1904/16/31, 1899/04/20, 1960/15/14, 2000/07/35, 1993/04/07")

# Creates the regex search and assigns it to 'match'.
# (x|y) means to search for x or y.
# (x[y]|z[w]) searches for xy or zw, allows for more specificty in regards to dates. eg limits to 31st.
match = re.search(r'(19|20)\d\d(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])', dates)
# Only returns first instance of matching date.
# Doesn't exlude some invalid dates, eg 30th of Feb.

# Prints the matches found.
print match.group()


# Years to match: 1900 - 2099 ??
# Months and days: 01, 03, 05, 07, 08, 10, 12; 01-31
#				   02; 01-28/29?
#				   04, 06, 09, 11; 01-30
# match year/match month/depending on month match day

# m1 = re.search(r'(19|20)\d\d/(0[1-9]|1[012])/(?(2=01,03,05,07,08,10,12)0[1-9]|12[0-9]|3[01])(?(2=02)0[1-9]|12[0-9])(?(2= 04, 06, 09, 11)0[1-9]|12[0-9]|3[1])', dates)
