#label = ('University', 'Hosting Partner', 'Non-Hosting Partner')

### Original attempt ...
#~ G.add_nodes_from(range(len(nodes)))
#~ G.add_weighted_edges_from(weighted)

# Draws the nodes of the network, colour nodes by type, label for use in legend.
#~ nx.draw_networkx_nodes(G, pos,
						#~ nodelist=[0, 1], 
						#~ node_color='b',
						#~ label = "University", 
						#~ node_size = NodSizs*1000)
#~ nx.draw_networkx_nodes(G, pos,
						#~ nodelist=[2, 3, 4],
						#~ node_color='r',
						#~ label = "Hosting Partner",
						#~ node_size = NodSizs*1000)
#~ nx.draw_networkx_nodes(G, pos,
						#~ nodelist=[5],
						#~ node_color='g',
						#~ label = "Non-hosting Partner",
						#~ node_size = NodSizs*1000)

# Draw the edges of the network, weigh links by number of interactions.			
#~ nx.draw_networkx_edges(G, pos,
						#~ edgelist= connect,
						#~ width = list(weights))

# Add names of institutions to nodes						
#~ nx.draw_networkx_labels(G, pos, labels, font_size=16)

# Add legend indicating colours, only a single representation of each type and scaled to be smaller.
#~ plt.legend(loc = (0,0), scatterpoints=1, markerscale = 0.2)

# Removes the axes.
#~ plt.axis('off')

# Save netowrk as .svg
#plt.savefig("../Results/Nets_output.svg")

# Issue trying to save, doesn't reproduce plot that is shown. Also file is massive.??
